# Client-Servidor

En aquesta guia s'explicarà a manera manual d'usuari els passos necessaris per a crear una connectivitat cap a l'escriptori d'un alumne per a poder supervisar el treball de l'aula en temps real, sobre l'entorn de virtualització Isard VDI.

En el primer punt, es documenta com establir una connexió entre l'escriptori d'un professor i el d'un alumne, formant així una estructura client-servidor. Tots dos rols es connectaran mitjançant una mateixa xarxa interna amb la qual el client pugui accedir a l'àrea del treball del servidor.

Es crearà un entorn el més real possible per a ajustar la guia a la pràctica. Es crearan uns usuaris i uns escriptoris amb els quals treballar. Si l'entorn amb escriptoris, plantilles i usuaris ja està creat, s'han de realitzar únicament els passos que com a administrador, professor i alumne faltin per realitzar.


## Com a usuari administrador

### Creació d'usuaris i grups

Per a començar es creen uns usuaris de prova amb els quals realitzar la pràctica. El més recomanat és que ja estiguin creats els grups als quals vagin a pertànyer abans de crear els usuaris.

Per a crear un grup, es va al panell d'Administració i en l'apartat de "Users".

En l'apartat de "Groups" es prem el botó ![](./client_server.images/client_server1.png)

![](./client_server.images/client_server2.png)

I s'emplena el formulari, en el nostre cas per a l'exemple hem creat un grup anomenat "aula_100".

![](./client_server.images/client_server3.png)

L'usuari Administrator crea un usuari professor de rol Advanced, una aula de classe i quatre alumnes rol Users amb aquesta informació:

![](./client_server.images/client_server4.png)

![](./client_server.images/client_server5.png)


### Interfícies de xarxa

Perquè els alumnes i el professor estiguin connectats mitjançant una xarxa interna i estableixin mitjançant aquesta xarxa la connectivitat, es connecten l'escriptori de l'alumne i el del professor amb la mateixa interfície de xarxa.
Es pot usar una de les interfícies de xarxa interna ja creades que ofereix Isard.

L'administrador es dirigeix al panell Administració i en el desplegable "Domains", es prem el botó "Resources". 

![](./client_server.images/client_server6.png)

En aquest apartat es mostra un llistat de totes les interfícies de xarxa ja creades en el sistema. Existeixen les interfícies manera Network (que usarà l'escriptori del professor) i manera OpenVSwitch (que usarà l'escriptori del professor i l'escriptori de l'alumne).

![](./client_server.images/client_server7.png)


### Compartir interfície de xarxa

Perquè els usuaris puguin utilitzar les xarxes, s'han de compartir. La primera interfície, "Default", ja està compartida per defecte amb tots els usuaris del sistema, així que només s'ha de compartir la segona interfície, "Private 1".

Es prem el botó ![](./client_server.images/client_server8.png) i sortirà una finestra de diàleg amb un formulari a emplenar.

Assignant el grup "aula_100", es compartirà amb tots els usuaris d'aquest mateix grup:

![](./client_server.images/client_server9.png)


### Creació d'escriptoris

En aquest apartat es procedeix a crear dos plantilles respectives per al professor i l'alumne que es compartiran també respectivament. Perquè així els usuaris puguin crear escriptoris amb els quals treballar sobre la base de la plantilla modificada que se'ls ha preparat. Es poden utilitzar altres plantilles ja creades en l'entorn atès que la configuració de les xarxes anirà en l'escriptori; aquest pas es realitza en cas que ni professor ni alumne disposin de plantilles.


#### Escriptori del professor

Es crea la plantilla debian_profe des de per exemple un escriptori Debian 9.5.0, on al seu torn es comparteix amb el professor test_docent01 i s'assignen les interfícies de xarxa Default i xarxa interna:

![](./client_server.images/client_server10.png)


#### Escriptori dels alumnes

Finalment es crea la plantilla ubuntu_alum des de per exemple un escriptori Ubuntu 20.04, la qual es comparteix amb el professor test_docent01 i amb el grup aula_100 i s'afegeix la interfície Xarxa interna:

![](./client_server.images/client_server11.png)


## Com a usuari professor

### Creació d'escriptoris

Mitjançant el login del professor test_profe01 es creen dos escriptoris sobre la base de la plantilla compartida amb els quals es realitzarà la prova de connexió. Un escriptori per al professor i un altre per als alumnes, creats amb les seves respectives plantilles, que reben el nom de servidor_web (professor) i client_web (alumne)

La creació d'escriptoris sobre la base de plantilla s'expliquen en l'apartat de "User" en el manual.

**Els escriptoris recentment creats tindran els mateixos paràmetres de maquinari amb els quals es va crear la plantilla; no es necessitarà modificació atès que venen preconfiguradas.**

![](./client_server.ca.images/client_server1.png) 


## Connexió entre escriptoris

Encara que hi ha moltes, aquí s'expliquen dues maneres de configurar les xarxes dels escriptoris: una mitjançant interfície gràfica i una altra mitjançant línia de comandos o terminal.

Una vegada creats s'arrenquen i s'accedeix a tots dos mitjançant els visors.

![](./client_server.images/client_server12.png) 


### Configuració del servidor

De manera automàtica en la primera interfície de xarxa de servidor_web s'ofereix una adreça IP mitjançant el servei DHCP de la xarxa exterior, per la qual cosa la màquina ha de tenir accés a Internet per defecte.

Per a configurar la segona interfície amb xarxa interna se li assigna una adreça IP de manera manual.

A partir d'aquí:

#### Interfície gràfica

En Paràmetres, es prem el botó ![](./client_server.images/client_server13.png), i en l'apartat IPv4 es configura la xarxa d'aquesta manera:

![](./client_server.ca.images/client_server14.png)

![](./client_server.ca.images/client_server15.png)

![](./client_server.ca.images/client_server16.png)

![](./client_server.ca.images/client_server17.png)

Es pot comprovar fàcilment obrint una terminal i escrivint el comando 

``` 
ip -c a 
```

Retorna una sortida semblant a aquesta:

![](./client_server.images/client_server18.png)


#### Línia de comandos

Per a fer-ho amb la terminal s'usa el paquet network-manager que ha d'instal·lar-se en la màquina, i mitjançant el programari nmcli i els comandos següents es configura la segona interfície, on el valor nomeni_interfície correspon a la targeta de xarxa que volem modificar.

Si el nom no és clar, amb el següent comando es pot comprovar:

```
sudo nmcli connection show
```

![](./client_server.images/client_server19.png)

```
sudo nmcli connection down nombre_interfaz
sudo nmcli connection modify nombre_interfaz ipv4.method manual ipv4.address 192.168.200.10/24
sudo nmcli connection up nombre_interfaz
```

Amb el mateix comando comprovem la modificació de la interfície:

```
ip -c a
```

![](./client_server.images/client_server20.png)


### Configuració del client

Se li assigna a continuació una adreça IP a client_web per a la xarxa interna, d'aquesta manera per a Ubuntu:

#### Interfície gràfica

En l'apartat Configuració de xarxa cablejada:

![](./client_server.es.images/client_server21.png)

![](./client_server.es.images/client_server22.png)

![](./client_server.es.images/client_server23.png)

Amb el mateix comando es fa la comprovació:

![](./client_server.images/client_server24.png)

#### Línia de comandos

El valor nom_interfície correspon a la targeta de xarxa que volem modificar.

Si el nom no és clar, amb el següent comando es pot comprovar:

```
sudo nmcli connection show
```

![](./client_server.images/client_server25.png)

S'obre un terminal i mitjançant el paquet "network-manager" es configura la interfície:

```
sudo nmcli connection down nombre_interfaz
sudo nmcli connection modify nombre_interfaz ipv4.method manual ipv4.addresses 192.168.200.11/24
sudo nmcli connection up nombre_interfaz
```

Amb el mateix comando comprovem la modificació de la interfície:

```
ip -c a
```

![](./client_server.images/client_server26.png)


### Prova de connexió

#### Comando ‘ping’

Per a comprovar la correcta comunicació entre escriptoris, amb el comando ping s'envia paqueteria cap a l'adreça IP o domini descrita per a comprovar una connexió entre totes dues màquines. Es realitza el comando en totes dues màquines i retorna així una resposta com aquesta, comprovant que la connexió existeix i es comuniquen:

![](./client_server.images/client_server27.png)

#### Entorn web amb ‘Apache’

Per a acabar, es pot provar també mitjançant el servidor d'HTTP Apatxe que s'estableix connexió.
S'instal·la mitjançant terminal en l'escriptori servidor_web i es connecta client_web al programari mitjançant el navegador web de la màquina.

![](./client_server.ca.images/client_server28.png)

Es comprova la instal·lació d'Apatxe i es visualitza l'existència del fitxer HTML de comprovació que mostra el servei quan et connectes a ell:

![](./client_server.ca.images/client_server29.png)

El client_web només ha d'obrir un navegador web i escriure l'adreça IP de la màquina del servidor perquè Apatxe respongui:

![](./client_server.images/client_server30.png)
# Windows Server 2019 Installation Guide

The intention of this guide is to show step by step how to create a Windows Server 2019 base image to create a template adapted to the IsardVDI desktop virtualization environment.

The idea of this document is to provide and collect ideas about what Windows elements to modify and customize for better performance.


## Pre-installation Windows Server 2019

### Desktop Creation

1.- Create *new desktop* from the main panel

2.- Apply *selected *viewers**, *Default** and *VPN** networks and mark *desktop boot from ISO image*

![](./guide)install](ws2019.images/dWNL9cl.png)


3. Create the desktop based on the *Windows Server 2019 ISO install* template

![create_desktop)install](./guide)install)ws2019.images/uJIyLj9.png)


4.- The *Media** section of the desktop will be added the images that the template brings, such as the *Windows Server installer 2019* and the * VirtIO drivers* with the installation agent("win2019 eval" and "virtio-win" respectively)

![](./guide)install](ws2019.images/EHwqTEc.png)


5. Once created, *boot desktop*


## Installation

### Operating System

1. If this screen exits at boot, *Send "Ctr+Alt+Del"*

![](./guide)install](ws2019.images/8oqNxy8.png)

![](./guide)install](ws2019.images/JG4kD1Q.png)

![](./guide)install](ws2019.images/dHk0hkw.png)


2.- Choose *"Library Experience"* for *"Standard Evaluation"*

![](./guide)install](ws2019.images/OVUbe6T.png)


3.- Choose *custom install*

![](./guide)install](ws2019.images/h4LGdaT.png)


### Load drivers


1. Now you have to *load VirtIO drivers* so that the operating system recognizes it as a *valid disk type*. Clicking "Find Driver"

![](./guide)install](ws2019.images/CZm4guM.png)


2. Searches the *removable disk drive* corresponding to the *virtio-win** image in the browser and removes the corresponding file for Windows Server 2019

![](./guide)install](ws2019.images/VvDyAED.png)

![](./guide)install](ws2019.images/bm5lrRc.png)

![](./guide)install](ws2019.images/0Nyv2cG.png)

![](./guide)install](ws2019.images/qwgRZDo.png)


3.- *Show** connection error details and *accept**

![](./guide)install](ws2019.images/9I0BEIi.png)

![](./guide)install](ws2019.images/iSCDywB.png)


4.- Click *"Siguiente"** and *start operating system installation*

![](./guide)install](ws2019.images/oFiNaaf.png)


## Post-installation

### Desktop Modify

1.- *Change desktop boot* from ISO to boot with *hard disk*

![](./guide)install](ws2019.images/dpBb8Pw.png)

![](./guide)install](ws2019.images/acLq1Ng.png)


2. Booting desktop


3. The first one to open will be this panel where the password should be changed. In this case in **Aneto_3404***

![](./guide)install](ws2019.images/YPy7q5M.png)


4.- *Send** again combination *"Ctrl+Alt+Del"**

![](./guide)install](ws2019.images/beVwtF5.png)


### Driver Installation

1.- Opens the *"virtio-win"** image again and runs the file to *install agent* and the required VirtIO drivers, such as network components

![](./guide)install](ws2019.images/ipRyw14.png)

![](./guide)install](ws2019.images/Kt5h8a1.png)

![](./guide)install](ws2019.images/9Fhlfg8.png)

![](./guide)install](ws2019.images/jwPcjg4.png)

![](./guide)install](ws2019.images/8dPDa6i.png)

![](./guide)install](ws2019.images/e7PGTOF.png)


2.- *rebooting equipment* and installing *'virtio-win-guest-tools'**, where you have *'qemu-guest-agent'**, which will allow you to communicate your desktop system with *viewer**, and thus recognize the *screen resizing* or the reading of *USB devices* from the host* machine

![](./guide)install](ws2019.images/S9tH3vy.png)

![](./guide)install](ws2019.images/VPJ1kaY.png)


### Remote Desktop

1.- Enable *remote desktop* in *'Settings'** system

![](./guide)install](ws2019.images/OmAPWKi.png)

![](./guide)install](ws2019.images/zcExQev.png)

![](./guide)install](ws2019.images/tglLC70.png)


2.- To *Advanced Settings*:

![](./guide)install](ws2019.images/kK4hnmR.png)

![](./guide)install](ws2019.images/YD6EGoK.png)


### Network Verification

Check *correct** *networks** configured with ```ipconfig``` command by opening a CMD

![](./guide)install](ws2019.images/4IPIky9.png)


### Login for RDP VPN viewer

Modifying your desktop and *activating the “RDP VPN” viewer*, and *changing credentials* to make them *sames** the user and point out that the *Windows logo*.

This way it will be *connected automatically by RDP without validation*.

(Following https://learn.microsoft.com/en-us/troubleshoot/windows-server/user-profiles-and-logon/turn-on-automatic-logon)

![](./guide)install](ws2019.images/957mTsW.png)


## Create T3

*T3 templates* are the *Windows** templates where Optimization Tools** software has been *passed.
This *remove** *unused services* that fill *space** on disk and memory and *embed** power management.
*Enhance** thus *performance** the entire desktop.

The manual steps here explain *creating the T1 template*, where after the installation finishes the images are removed from the *Media** section and converted to the T1.- template

At this last point the software is run to *create the T3*.


### Optimization tools

Run the *Optimization Tools* tool as [explains in this manual section](https://isard.gitlab.io/isardvdi-docs/install/win10'installguiguide.ca/#vmware-us-optimization-tool) to create a T3 template, which Isard is so named after after a desktop passes through Optimization tools software.

1. There is an already created image that needs to be *added to the T1* and *create a desktop* to be able to *run the program*, and then *create the template named T3*

![](./guide)install](ws2019.images/Hx8YIMy.png)


2.- *Run** desktop and run software

![](./guide)install](ws2019.images/uV9Pgy9.png)


3. This window opens and we click *'Analyze'**

![](./guide)install](ws2019.images/zTnYuQV.png)


4.- To *'common options'* these *parameters** are marked

![](./guide)install](ws2019.images/yE5LsHe.png)

![](./guide)install](ws2019.images/WbZhJFc.png)


5.- Click *'Optimize'** to start optimization


6.- *restarting** desktop to check for proper functioning and see that the system has *improved** speed


7. - Finally the desktop is turned off, modified and *moving image from "Media"* paragraph


8. On this desktop *create a new T3 template*
# Ubuntu 22.04 Jammy Jellyfish install (GNOME and MATE)

## Instal·lació del SO

**Selecció de l'idioma i de la distribució de teclat**

![](installation.images/AS8jZEE.png)

![](installation.images/f40MQuq.png)

**Tipus d'instal·lació**

![](installation.images/MEKs27m.png)

**Taula de particions**

![](installation.images/EuBTfMe.png)

![](installation.images/iTJXQzu.png)

![](installation.images/GtfOE2N.png)

![](installation.images/UG3wCAI.png)

![](installation.images/zG4r8B4.png)

![](installation.images/MihtaDe.png)


## GNOME
### Configuració
#### Terminal

**Comandaments bàsic**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim vlc gedit
```

**Reduir de la memòria swap al sistema**
```
$ sudo sysctl vm.swappiness=1
```

**Deshabilitar actualitzacions automàtiques**
```
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
```

#### Paràmetres

**Privacitat - Pantalla**

![](installation.images/ho65Ywk.png)

**Compartició - Escriptori remot**

![](installation.images/lyLpsBq.png)

Es deixa buida la nova contrasenya

![](installation.images/XcA0OFL.png)

**Aplicacions per defecte**

![](installation.images/TK9QqC3.png)

**Quant a - Actualitzacions de programari**

![](installation.images/KxegVsP.png)


### Personalització
#### Terminal

**Canviar el fons de pantalla del login**

```
$ sudo apt install libglib2.0-dev-bin
$ wget -qO - https://github.com/PRATAP-KUMAR/ubuntu-gdm-set-background/archive/main.tar.gz | tar zx --strip-components=1 ubuntu-gdm-set-background-main/ubuntu-gdm-set-background
$ sudo ./ubuntu-gdm-set-background --image /PATH/TO/YOUR/IMAGE
```


#### Paràmetres

**Fons**

![](installation.images/fg1dqsP.png)

**Usuaris**

![](installation.images/xbfvkg9.png)

![](installation.images/xzMI5GX.png)


## MATE
### Configuració
#### Terminal

**Comandaments bàsics**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim vlc gedit
```

**Reduir la memòria swap al sistema**
```
$ sudo sysctl vm.swappiness=1
```

**Deshabilitar les actualitzacions automàtiques**
```
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
```
**Habilitar l'auto-login  de l'usuari**
```
$ sudo vim /usr/share/lightdm/lightdm.conf.d/50-arctica-greeter.conf
# add next line to the end of file
autologin-user=isard
```

**Habilitar l'accés via RDP**
```
$ sudo apt install xrdp
$ sudo systemctl enable xrdp
$ sudo systemctl restart xrdp

$ sudo vim /etc/X11/Xsession.d/80mate-environment
# add next line before 'fi' closes, last line inside conditional
unset DBUS_SESSION_BUS_ADDRESS
```


#### Paràmetres del sistema

**Administració - Programari i actualitzacions**

![](installation.images/mfglH6f.png)

**Maquinari - Gestor d'energia**

![](installation.images/LM9q2Yz.png)

**Aspecte i comportament - Estalvi de pantalla**

![](installation.images/zH0YWLI.png)

**Personal - Aplicacions preferides**

![](installation.images/jyXwMBE.png)


### Personalització
#### Terminal

**Canviar el fons de pantalla del login**

```
$ sudo vim /usr/share/glib-2.0/schemas/30_ubuntu-mate.gschema.override
background='/path/to/wallpaper'
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```


#### Paràmetres del sistema

**Aspecte i comportament - Aparença - Fons**

![](installation.images/xE9czAP.png)


**Personal - Quant a mi**

![](installation.images/eAwSqAf.png)


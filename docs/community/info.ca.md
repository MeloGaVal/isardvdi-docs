# Comunitat IsardVDI

Benvingut a la comunitat IsardVDI! Aquesta pàgina està dissenyada per proporcionar recursos i suport als usuaris i col·laboradors de la plataforma IsardVDI.

## Començant

Si sou nou a IsardVDI, aquí teniu alguns recursos per ajudar-vos a començar:

- [Documentació IsardVDI](https://docs.isardvdi.com/en/latest/): La documentació oficial de l'IsardVDI, que inclou instruccions d'instal·lació, guies d'usuari i documentació de desenvolupador.
- [Repositori GitLab IsardVDI](https://gitlab.com/isardvdi/isard): El repositori de codi font per a IsardVDI, on trobareu els últims llançaments, el codi de contribució i els problemes d'informe.
- Fòrum IsardVDI (en construcció): Un fòrum comunitari per a usuaris i col·laboradors d'IsardVDI, on podeu fer preguntes, compartir idees i obtenir suport de la comunitat.

## Contribuint a IsardVDI

IsardVDI és un projecte de codi obert, i acollim amb satisfacció les contribucions de qualsevol persona interessada a ajudar a millorar la plataforma. Aquí hi ha algunes maneres d'involucrar-vos:

- [Incidències del GitLab](https://gitlab.com/isardvdi/isard/-/issues): Si trobeu un error o teniu una petició de funcionalitat, podeu informar-lo sobre problemes de GitLab. Aquest també és un bon lloc per a trobar qüestions que necessiten ajuda de la comunitat.
- [Peticions de fusió de GitLab](https://gitlab.com/isardvdi/isard/-/merge_requests): Si voleu contribuir amb el codi a IsardVDI, podeu enviar una petició de fusió a GitLab. Abans de fer-ho, llegiu les nostres [directrius de col·laboració](https://gitlab.com/isardvdi/isard/-/blob/master/CONTRIBUTING.md).
- Fòrum comunitari (en construcció): Si teniu preguntes sobre com contribuir a l'IsardVDI, o voleu discutir una possible contribució, podeu utilitzar la categoria de desenvolupament del nostre fòrum per a connectar-vos amb altres col·laboradors.

## Orientacions de la comunitat

Volem fomentar una comunitat acollidora i solidària entorn de IsardVDI. A aquest efecte, demanem que tots els membres de la comunitat compleixin les següents directrius:

- Sigueu respectuosos: Donem la benvinguda a la diversitat i la inclusivitat de la nostra comunitat, i demanem que tothom es tracti amb respecte i amabilitat.
- Sigueu constructius: quan oferiu comentaris o crítiques, si us plau, feu-ho de manera constructiva i útil.

## Conclusió

Esperem que trobis aquesta pàgina útil per començar i contribuir a IsardVDI. Gràcies per formar part de la nostra comunitat!
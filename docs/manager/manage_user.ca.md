# Gestió d'usuaris

Per a gestionar els usuaris s'ha d'anar al panell d'Administració, es prem el botó ![](./manage_user.ca.images/manage_user1.png)

![](./manage_user.ca.images/manage_user2.png)

Es prem el botó ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Creació de Grups

Per a crear grups s'ha d'anar a la subsecció "Management" sota la secció "User" al panell d'"Administració". S'ha de cercar la taula "Groups" i prémer el botó ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

I s'emplenen els camps del formulari.


![](./manage_user.images/manage_user3.png)

!!! Info "Notes importants respecte als camps"
    * "**Linked groups**": Tots els recursos compartits amb els seus grups enllaçats s'heretaran automàticament pel grup creat. Per exemple, si un Grup A es crea amb Grup B com a grup enllaçat, tots els recursos compartits amb el Grup B també es compartiran amb el Grup A.

    * "**Auto desktops creation**": Quan un usuari pertanyent al grup accedeix al sistema se li crearà automàticament un escriptori amb la plantilla seleccionada

    * "**Ephimeral desktops**": Establirà un temps límit d'ús dels escriptoris temporals. (Per a això també s'ha de configurar, sent Admin, en Config el "Job Scheduler")


## Edició de grups

Es poden editar els paràmetres d'un grup prement la icona ![](./manage_user.images/manage_user13.png) al costat del grup que es vol actualitzar, llavors es prem ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png)

Una finestra de diàleg s'obrirà amb els mateixos paràmetres que la finestra de creació.

![](./manage_user.images/manage_user29.png)

## Creació d'Usuaris

Per crear usuaris s'ha d'accedir a la subsecció "Management" sota la secció "Usuari" al panell d'"Administració".

### Individualment

Es cerca la taula "Users" i es prem el botó ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user6.png)

I s'emplena els camps.

!!! Info "Notes importants respecte als camps"
    * "**Secondary groups**": Tots els recursos que han estat compartits amb els seus grups secundaris s'heretaran automàticament per l'usuari creat. A més, l'usuari serà afegit a tots els desplegaments creats a qualsevol dels seus grups secundaris.

### Creació massiva

Es cerca la taula "Users" i es prem el botó ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user9.png)

Es pot descarregar un arxiu d'exemple prement el botó ![](./manage_user.images/manage_user10.png). Sortirà un formulari a emplenar:

!!! Warning "Evitant errors"
    * Els camps "**category**" i "**group**" han d'adir-se **exactament** amb el seu nom
    * Es recomana la codificació **Unicode (UTF-8)**
    * L'arxiu csv ha d'estar separat per **comes** ","
    * És molt recomanable fer servir el csv d'exemple

![](./manage_user.images/manage_user11.png)

Una vegada emplenat, es pot pujar en 

![](./manage_user.images/manage_user12.png)

I si s'ha pujat correctament, sortirà una taula de previsualització dels usuaris:

![](./manage_user.images/manage_user28.png)

!!! Failure "Errors"
    Si l'arxiu csv no s'ha pujat correctament, es mostrarà un error indicant la raó.

## Edició d'usuaris

Per a editar usuaris s'ha d'anar a la subsecció "Management" sota la secció "User" al panell d'"Administració".

### Individualment

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol actualitzar, llavors es prem ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png)

Una finestra de diàleg s'obrirà amb els mateixos paràmetres que la finestra de creació.

![](./manage_user.images/manage_user35.png)

!!! Info "Notes importants respecte als camps"
    * Els camps "**username**", "**group**" i "**category**" no es poden modificar.

### Edició massiva

Es cerca la taula "Users" i es prem el botó ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user9.png)

On es podrà pujar un arxiu csv amb les dades actualitzades. Llavors es selecciona la casella "Update existing users".

![](./manage_user.images/manage_user36.png)

!!! Warning "Evitant errors"
    * Els camps "**username**", "**category**" i "**group**" han d'adir-se **exactament** amb el seu nom per tal d'actualitzar l'usuari
    * Es recomana la codificació **Unicode (UTF-8)**
    * L'arxiu csv ha d'estar separat per **comes** ","

!!! Info "Notes importants respecte als camps"
    * Els camps "**username**", "**group**" i "**category**" no es poden modificar. La resta de camps s'actualitzaràn
    * Els usuaris seran afegits a tots els grups secundaris seleccionats

### Habilitar/Deshabilitar Usuari

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol habilitar/deshabilitar, llavors es prem ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png)

L'estat d'un usuari es pot veure a la taula d'usuaris.

![](./manage_user.images/manage_user38.png)

!!! Danger "Aneu amb compte"
    * Si l'usuari es deshabilita mentre accedeix al sistema es poden produir errors de sessió.

### Canviar Contrasenya d'Usuari

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari el qual es vol canviar la contrasenya, llavors es prem ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png)

I sortirà una finestra de diàleg amb el següent formulari:

![](./manage_user.images/manage_user40.png)

!!! Tip "Suggeriment"
    * L'usuari pot canviar la seva contrasenya mitjançant el seu [perfil](../user/profile.ca.md).

### Suplantar Usuari

Es poden editar els paràmetres d'un usuari prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que es vol suplantar, llavors es prem ![](./manage_user.images/manage_user44.png).

![](./manage_user.images/manage_user41.png)

!!! Danger "Avís d'exempció de responsabilitat"
    Suplantar un usuari **proporciona l'accés a totes les seves dades i escriptoris i comporta riscos inherents**. Abans de continuar, és important **considerar la sensibilitat de la informació a la qual s'accedirà**.

## Clau d'autoregistre

A la taula de "Grups" cal cercar el grup del qual es vol obtenir el codi. Es prem el botó ![](./manage_user.images/manage_user13.png) per veure els detalls del grup

![](./manage_user.images/manage_user14.png)

El grup es desplega amb unes opcions. Es prem el botó ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

Una finestra de diàleg s'obrirà on es poden generar claus d'inscripció fent clic a les diferents caselles de selecció. 

![](./manage_user.images/manage_user32.png)

Un cop generada la clau d'inscripció, es pot copiar i compartir amb els usuaris.

![](./manage_user.images/manage_user17.png)

!!! Danger "Aneu amb compte"
    * En registrar-se, **als usuaris se'ls donarà el rol de la clau d'inscripció compartida**. Per exemple, als professors se'ls donarà el codi "Advanced" i als estudiants el codi "Users".
    * Una quantitat il·limitada d'usuaris poden registrar-se utilitzant la clau d'inscripció proporcionada. Per tant, es recomana desactivar-les un cop fetes servir.

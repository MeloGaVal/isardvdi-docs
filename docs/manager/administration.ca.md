# Administració

!!! info "Rols amb accés"

    Només els **gestors** (managers) i els **administradors** tenen accés a totes les característiques.
    Els gestors estan restringits a la seva pròpia categoria.

La interfície web d'administració permet als gestors i administradors configurar i gestionar funcions avançades i gestionar tots els usuaris del sistema, escriptoris, plantilles i mitjans.

Els usuaris de rols d'administració també tenen accés a descàrregues IsardVDI, gestió d'hipervisors i configuració del sistema.

Al  [**Manual d'administrador**](../../admin/index.ca/) trobareu aquestes prestacions.
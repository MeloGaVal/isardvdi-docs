# Introduction

!!! info "Role access"

    Only **managers** and **administrators** have access to this feature.
    Managers are restricted to it's own category.

This administrators guide is organized as the web menu. The *manager roles* have restricted options and can only access data from it's own category:


![](domains.images/global_view.png)

Administrator roles have access to more options in the menu:

![](hypervisors.images/three_hypers.png)


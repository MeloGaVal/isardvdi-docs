# Introducción

!!! info "Roles con acceso"

    Solo los **gestores** (managers) y los **administradores** tienen acceso a esta característica.
    Los gestores están restringidos a su propia categoría.

Esta guia para administradores está organizada tal como el menú de la web. Los *roles gestores* (manager) tienen opciones restringidas y solo pueden acceder a los datos de su propia categoría:

![](domains.images/global_view.png)

Los roles administradores tienen acceso a más opciones en el menú:

![](hypervisors.images/three_hypers.png)


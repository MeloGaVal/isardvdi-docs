# IsardVDI Architecture Documentation

## Component Architecture Diagram

```mermaid
graph LR
    isard-portal["isard-portal (haproxy)"]
    isard-api["isard-api (python, jwt)"]
    isard-engine["isard-engine (python)"]
    isard-hypervisor["isard-hypervisor (libvirt/KVM)"]
    isard-webapp["isard-webapp (python)"]
    isard-vpn["isard-vpn (wireguard, ovs, guacmole)"]
    isard-authentication["isard-authentication (golang, jwt)"]
    isard-static["isard-static (vue)"]
    isard-squid["isard-squid (squid, spice tunnel)"]
    isard-websockify["isard-websockify (squid, vnc html5)"]
    isard-db["isard-db (rethinkdb)"]
    storage(storage)
    client(client)

    client --> |TCP 80,443| isard-portal
    client --> |UDP 443| isard-vpn
    isard-portal --> |HTTP| isard-static
    isard-portal --> |HTTP| isard-api
    isard-portal --> |HTTP| isard-webapp
    isard-portal --> |HTTP| isard-squid
    isard-portal --> |HTTP| isard-websockify
    isard-portal --> |HTTP| isard-authentication
    isard-portal --> |HTTP| isard-grafana
    isard-portal --> |HTTP| isard-storage
    isard-squid --> |TCP| isard-hypervisor
    isard-websockify --> |TCP| isard-hypervisor
    isard-hypervisor --> |Docker Bind Mount| storage
    isard-engine --> |ReQL| isard-db
    isard-api --> |ReQL| isard-db
    isard-api --> |RQ| isard-redis
    isard-webapp --> |ReQL| isard-db
    isard-engine --> |SSH| isard-hypervisor
    isard-vpn --> |IP| isard-hypervisor
    isard-backupninja --> |ReQL| isard-db
    isard-backupninja --> |Docker Bind Mount| storage
    isard-authentication --> |ReQL| isard-db
    isard-portal --> |HTTP| isard-guac
    isard-guac --> |HTTP| isard-api
    isard-guac --> isard-vpn
    isard-stats --> |ReQL| isard-db
    isard-stats --> |InfluxQL| isard-influxdb
    isard-stats --> |SSH| isard-hypervisor
    isard-grafana --> |InfluxQL| isard-influxdb
    isard-storage --> |Docker Bind Mount| storage
    isard-storage --> |RQ| isard-redis
```

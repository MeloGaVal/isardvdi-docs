# Inici ràpid

Segueix aquestes instruccions per a clonar el repositori i activar IsardVDI:

```bash
git clone https://gitlab.com/isard/isardvdi
cd isardvdi
cp isardvdi.cfg.example isardvdi.cfg
./build.sh
docker-compose pull
docker-compose up -d
```

Esperi un minut la primera vegada, ja que la base de dades es completarà abans que l'inici de sessió de IsardVDI estigui disponible.

Navega a <https://localhost>. L'usuari predeterminat és **admin** i la contrasenya predeterminada és **IsardVDI**.

Es pot descarregar immediatament imatges del sistema operatiu preinstal·lades i optimitzades des del menú *Descàrregues* i usar-les, crear plantilles i molt més.

Si us plau, llegeixi la resta de la documentació per a gaudir de totes les funcions.
# Requirements

You only need to have docker service and docker-compose installed. Example with Debian 10.

- Install **Docker**: https://docs.docker.com/engine/installation/

    - Note: docker 17.04 or newer needed for docker-compose.yml v3.2


```bash
apt-get remove docker docker-engine docker.io containerd runc
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io
```

- Install **docker-compose**: https://docs.docker.com/compose/install/

    - Note: docker-compose 1.12 or newer needed for docker-compose.yml v3.5. You can install last version using pip3:

```bash
apt install python3-pip -y
pip3 install docker-compose
```

**NOTE**: Your hardware needs to have virtualization enabled. You can check that in your BIOS but also from CLI:

```
egrep ‘(vmx|svm)’ /proc/cpuinfo
```
​	If you see nothing in the output your CPU has no virtualization capabilites or they are disabled in BIOS.
# Quick Start

Follow these instructions to clone the repository and bring IsardVDI up.

```bash
git clone https://gitlab.com/isard/isardvdi
cd isardvdi
cp isardvdi.cfg.example isardvdi.cfg
./build.sh
docker-compose pull
docker-compose up -d
```

Please wait a minute the first time as the database will get populated before the IsardVDI login becomes available.

Browse to <https://localhost>. Default user is **admin** and default password is **IsardVDI**.

You can immediately download preinstalled and optimized Operating System images from **Downloads** menu.

- Start **demo desktops** and connect to it using your browser and spice or vnc protocol. Nothing to be installed, but already secured with certificates.
- Install virt-viewer and connect to it using the spice client. **Sound and USB** transparent plug will be available.

Create your own desktop using isos downloaded from **Downloads** menu option or you can upload yours from **Media** menu option. When you finish installing the operating system and applications create a **Template** and decide which users or categories you want to be able to create a desktop identical to that template. Thanks to the **incremental disk creation** all this can be done within minutes.

<iframe src="https://drive.google.com/file/d/1tPL12yw3MEV5IEPL5by7z76zVVSNnAng/preview" width="640" height="480"></iframe>

Don't get tied to an 'stand-alone' installation in one server. You can add more hypervisors to your **pool** and let IsardVDI decide where to start each desktop. Each hypervisor needs only the IsardVDI hypervisor compose. Note that you should keep the storage shared between those hypervisors.

Please, read the rest of the documentation to enjoy all features.


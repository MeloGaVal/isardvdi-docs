# Plantilles

Una plantilla és un escriptori preconfigurat. El seu disc no es pot modificar, de manera que no es poden iniciar com escriptoris, però els seus paràmetres es poden personalitzar per satisfer les necessitats de cada usuari.

Les plantilles estan dissenyades per ser compartides amb altres usuaris perquè puguin crear els seus propis escriptoris.

!!! Info "Eliminar plantilles"
    Quan es crea una plantilla des d'un escriptori, el seu disc es duplica. Quan es crea un escriptori nou a partir d'una plantilla, es genera un disc nou en l'emmagatzematge que depèn del disc de la plantilla original. Per això els usuaris avançats no poden suprimir plantilles, ja que podria provocar la pèrdua de tots els escriptoris que en depenen.

Aquest és un exemple que il·lustra la relació de les plantilles i els discs:

**1.** Es crea un escriptori amb el disc d'emmagatzematge **D1**.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1]):::dk
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
  classDef dt stroke-width:2px
```

**2.** A continuació, es crea una plantilla a partir d'aquest escriptori. En crear la plantilla, el disc **D1** s'associarà amb la plantilla nova. Al mateix temps, es fa una còpia de **D1** i s'anomena **D1'**. Aquest disc copiat serà utilitzat per l'escriptori original en el futur, de manera que qualsevol canvi fet al disc original no l'afectarà.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.** Si es crea un escriptori nou a partir d'aquesta plantilla, es crea un disc nou **D2** per a l'escriptori nou. Aquest disc **D2** conté els canvis que es faran a l'escriptori respecte al disc de la plantilla **D1**. En altres paraules, **D2** està enllaçat a **D1** i l'escriptori obtindrà la seva informació de **D1** en el moment d'iniciar-lo.

Ës a dir, **D2** només conté els canvis fets a **D1** que són rellevants per a l'escriptori nou, i els dos discs romanen connectats entre si.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> dt2(Escriptori):::dt
  dt2 -.- dk3([D2]):::dk
  dk3 -- depèn de --> dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.** En duplicar una plantilla, no es crearà cap disc nou. En lloc d'això, la nova plantilla farà servir el mateix disc **D1** que la plantilla original.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> tp2(Plantilla):::tp
  tp2 -.- dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

Entendre la relació entre plantilles, escriptoris i discs és important per poder gestionar millor la infraestructura d'escriptoris virtual.
## Crear 

Per a crear una plantilla a partir d'un escriptori es prem la icona ![](./templates.es.images/templates1.png)

![](./templates.ca.images/templates1.png)

I la icona ![](./templates.es.images/templates3.png)

![](./templates.ca.images/templates2.png)

Se li pot assignar qualsevol nom/descripció, triar si es vol habilitar/deshabilitar (fer-la visible/invisible), i compartir-la amb grups/usuaris

![](./templates.ca.images/templates3.png)


## Les teves Plantilles

Per a veure les plantilles que has creat, es prem el botó ![](./templates.ca.images/templates13.png)

![](./templates.ca.images/templates4.png)

![](./templates.ca.images/templates5.png)


## Compartides amb tu

En aquest apartat es poden veure les plantilles que han estat compartides amb el teu usuari.

![](./templates.ca.images/templates6.png)

### Duplicar plantilla

Per a duplicar una plantilla i fer-la teva es prem la icona ![](./templates.images/templates14.png)

Es redirigeix a la pàgina on poder duplicar-la.

![](./templates.ca.images/templates14.png)

La duplicació d'una plantilla compartida crea una còpia on ets el propietari. Això et permet personalitzar la plantilla. Això inclou modificar els usuaris amb els quals es comparteix.

## Editar

Per a editar una plantilla es prem la icona ![](./templates.es.images/templates10.png)

![](./templates.ca.images/templates7.png)

I et redirigeix a la pàgina on poder editar la informació de l'escriptori, visors, maquinari, mitjana, etc.

![](./templates.ca.images/templates8.png)


## Compartir

Per a compartir una plantilla es prem la icona ![](./templates.es.images/templates13.png)

![](./templates.ca.images/templates9.png)

Apareixerà una finestra de diàleg on es pot compartir amb grups/usuaris

![](./templates.ca.images/templates10.png)


## Fer Visible/Invisible


### Visible

Per a fer visible una plantilla per als usuaris, es prem la icona ![](./templates.es.images/templates18.png)

![](./templates.ca.images/templates11.png)


### Invisible

Per a fer invisible una plantilla per als usuaris, es prem la icona ![](./templates.es.images/templates16.png)

![](./templates.ca.images/templates12.png) 
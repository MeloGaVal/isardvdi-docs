# Visor directe

Això serveix per a compartir un escriptori des d'un enllaç.

Es prem el botó ![](./direct_viewer.es.images/visor_directo1.png) de l'escriptori que es vulgui compartir per enllaç

![](./direct_viewer.ca.images/visor_directe1.png)

I a continuació es prem el botó ![](./direct_viewer.es.images/visor_directo3.png)

![](./direct_viewer.ca.images/visor_directe2.png)

Sortirà una finestra de diàleg on s'ha de seleccionar la casella perquè aparegui l'enllaç i poder copiar-lo

![](./direct_viewer.ca.images/visor_directe3.png)

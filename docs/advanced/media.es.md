# Medios

En este apartado se verán los medios creados por ti y los compartidos contigo.

Se pulsa el botón ![](./media.es.images/media1.png)

![](./media.es.images/media2.png)


## Tus Medios

En este apartado se pueden ver los medios que haya subido el usuario.

![](./media.es.images/media6.png)


## Compartidos contigo

En este apartado se pueden ver los medios que se hayan compartido con el usuario.

![](./media.es.images/media14.png)


## Subir Iso

Para subir una iso, se pulsa el botón ![](./media.es.images/media4.png)

![](./media.es.images/media3.png)

Redirecciona a una página donde se tiene que rellenar los campos:

![](./media.es.images/media5.png)


## Compartir

Para compartir una iso, se pulsa el icono ![](./media.es.images/media10.png)

Y aparecerá una ventana de diálogo dónde poder seleccionar si se quiere compartir por grupos/usuarios

![](./media.es.images/media11.png)


## Generar escritorio a partir de media

Para generar un escritorio a partir de una iso, se pulsa el icono ![](./media.es.images/media7.png)

![](./media.es.images/media8.png)

Y te redirecciona a una página donde se puede rellenar el formulario con el nombre, visores, etc.

![](./media.es.images/media9.png)


## Eliminar una iso

Para eliminar una iso, se pulsa el botón ![](./media.es.images/media12.png)

![](./media.es.images/media13.png)
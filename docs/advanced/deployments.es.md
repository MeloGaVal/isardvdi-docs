# Despliegues

Los despliegues son escritorios creados para otros usuarios.


## Como crear despliegues

Para crear un despliegue, se debe ir a la barra superior y se pulsa el botón ![](./deployments.es.images/deployments1.png)

![](./deployments.es.images/deployments2.png)

Se pulsa el botón ![](./deployments.es.images/deployments3.png)

![](./deployments.es.images/deployments4.png)

Se redirecciona a éste formulario; donde se puede rellenar con el nombre del despliegue, el del escritorio y una pequeña descripción. Se debe seleccionar una plantilla de la cuál queremos crear nuestros escritorios. Para hacerlos visible a los usuarios se pulsa el botón ![](./deployments.es.images/deployments5.png)

![](./deployments.es.images/deployments6.png)


## Como iniciarlos/pararlos

### Iniciarlos

Se tiene que entrar dentro del despliegue.

![](./deployments.es.images/deployments7.png)

Para iniciar un escritorio se pulsa el botón ![](./deployments.ca.images/deployments8.png)

![](./deployments.es.images/deployments8.png)

Una vez iniciado, detectará el "IP" del escritorio (si tiene), el "Estado" (Si está iniciado o parado), y se puede seleccionar el tipo de visor

![](./deployments.es.images/deployments9.png)

Se puede seleccionar el visor que interese pulsando el menú despleglable ![](./deployments.es.images/deployments10.png)


### Pararlos

Se tiene que entrar dentro del despliegue.

![](./deployments.es.images/deployments7.png)

#### Individualmente

Para parar un escritorio se pulsa el botón ![](./deployments.es.images/deployments12.png)

![](./deployments.es.images/deployments13.png)


#### Todos

Para parar todos los escritorios se pulsa el botón ![](./deployments.ca.images/deployments16.png)

![](./deployments.es.images/deployments14.png)


## Como ir al Videowall

Hace falta estar dentro del despliegue para poder pulsar el botón ![](./deployments.ca.images/deployments18.png)

![](./deployments.es.images/deployments15.png)

Y se redirecciona a esta página dónde se pueden ver los escritorios de los usuarios.

![](./deployments.es.images/deployments16.png)

Para interactuar con un escritorio se pulsa el botón ![](./deployments.ca.images/deployments21.png)

Y te redirecciona a 

![](./deployments.es.images/deployments17.png)


## Como hacer visible/invisible

### Visible

Se pulsa el botón ![](./deployments.ca.images/deployments23.png)

![](./deployments.es.images/deployments18.png)

Aparecerá una ventana emergente

![](./deployments.es.images/deployments19.png)

Una vez seleccionado "Visible", el despliegue quedará en verde

![](./deployments.es.images/deployments20.png)


### Invisible

Se pulsa el botón ![](./deployments.ca.images/deployments26.png)

![](./deployments.es.images/deployments21.png)

Aparecerá una ventana emergente

![](./deployments.es.images/deployments22.png)

Una vez seleccionado "Invisible", el despliegue quedará en rojo

![](./deployments.es.images/deployments23.png)


## Como recrear los escritorios

Esta función sirve para recrear los escritorios eliminados del despliegue.

Se pulsa el botón ![](./deployments.ca.images/deployments31.png)

![](./deployments.es.images/deployments24.png)

![](./deployments.es.images/deployments25.png)


## Como descargarse el fichero de visores directos

Éste fichero sirve para ver el listado de usuarios dentro del propio despliegue.

Para obtener este fichero, se pulsa el botón ![](./deployments.ca.images/deployments34.png)

Y aparece una ventana emergente

![](./deployments.es.images/deployments27.png)

En este archivo aparece un excel con el nombre de usuario, su nombre, su correo y un enlace que redirecciona el escritorio del usuario.

![](./deployments.ca.images/deployments36.png)


## Como eliminar un despliegue

Para eliminar un despliegue se pulsa el botón ![](./deployments.ca.images/deployments37.png)

![](./deployments.es.images/deployments28.png)
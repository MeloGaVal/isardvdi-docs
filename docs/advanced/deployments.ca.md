# Desplegaments

!!! Info
    Aquesta secció és només per a **administradors**, **gestors** i **usuaris avançats**


Els desplegaments són una característica potent que permet als usuaris avançats crear un conjunt d'escriptoris basats en una única plantilla. Tots els usuaris inclosos en un desplegament tindran accés al seu propi escriptori únic.

Amb els desplegaments, podeu crear i gestionar diversos escriptoris simultàniament per a tots els usuaris seleccionats. Com a creador, podeu gestionar fàcilment tots els escriptoris en un sol lloc a través de la secció Desplegaments. Fins i tot teniu la capacitat d'interactuar amb cada escriptori i veure les pantalles simultàniament.


## Com crear desplegaments

Per a crear un desplegament, heu d'anar a la barra superior i prémer el botó ![Botó superior de desplegaments](./deployments.ca.images/deployments1.png)

Redirigirà a aquesta pàgina:

![pàgina de desplegaments](./deployments.ca.images/deployments4.png)

Premeu el botó ![Desplegament nou](./deployments.ca.images/deployments3.png)

Se us dirigirà a un formulari on podreu introduir la informació necessària per al vostre desplegament, inclòs el nom del desplegament, cada nom d'escriptori que veuran els usuaris i una breu descripció.

![formulari de desplegament nou](./deployments.ca.images/deployments6.png)

A més, heu de seleccionar una plantilla des d'on crear els vostres escriptoris. Un cop creat, els vostres escriptoris es faran visibles per als usuaris, feu clic a ![no visible / visible](./deployments.ca.images/deployments5.png)

A més, heu d'especificar els usuaris que s'han d'incloure en el desplegament. A la secció final del formulari, podeu seleccionar el grup necessari d'usuaris, així com qualsevol usuari addicional.

!!! Note
    També es crearà un escriptori per al vostre usuari dins del desplegament

L'última secció són les opcions avançades. Aquí podeu canviar els visualitzadors dels escriptoris, el maquinari i la miniatura.

### Arrencada i aturada

Per accedir al desplegament, heu de fer clic al nom del desplegament.

![llista de desplegaments](./deployments.ca.images/deployments7.png)

Per a iniciar un escriptori, premeu el botó ![Iniciar](./deployments.ca.images/deployments8.png)

Un cop iniciat, detectarà la "IP" de l'escriptori (si en té), l'"Estat" (si està iniciat o aturat), i el tipus de visor es pot seleccionar

Podeu seleccionar el visor que us interessa prement el menú desplegable

![llista desplegable dels visors](./deployments.ca.images/deployments11.png)

Per aturar un escriptori iniciat, simplement feu clic al botó ![aturar](./deployments.ca.images/deployments14.png) que apareixerà. Això canviarà el seu estat a "Aturant". Si bé podeu utilitzar l'opció "Força l'aturada" per a una parada immediata, pot provocar problemes de tancament i problemes potencials.

Per a aturar tots els escriptoris, premeu el botó  ![icona d'aturada](./deployments.ca.images/deployments16.png)


## Videowall

El Videowall és una funció que permet veure totes les pantalles de tots els escriptoris d'un desplegament particular simultàniament i en temps real. També us permet interactuar amb qualsevol dels escriptoris mostrats i utilitzar-los com a propis.

!!! Warning
     Entrar a la pantalla d'un altre usuari prendrà el control de l'usuari propietari.
  
!!! Danger "Disclaimer"
    Tingueu en compte que el permís del propietari és essencial abans d'entrar al seu escriptori a causa de problemes de privadesa.

Per accedir al Videowall, feu clic al botó ![icona del videowall](./deployments.ca.images/deployments18.png)

Se us redirigirà a una pàgina on podeu veure els escriptoris de tots els usuaris.

![pantalla del videowall](./deployments.ca.images/deployments20.png)

Per a interactuar amb un escriptori, feu clic al botó  ![maximitza](./deployments.ca.images/deployments21.png) que us redirigirà a una vista a pantalla completa de l'escriptori seleccionat a pantalla completa.

![pantalla completa del videowall](./deployments.ca.images/deployments22.png) 


## Fer visible/invisible

Fer visible o invisible el desplegament significa que els usuaris dins del desplegament poden veure i accedir al seu escriptori o no.

### Visible

Per a fer-lo visible, feu clic al botó ![botó d'ull de creu](./deployments.ca.images/deployments23.png)

Apareixerà una caixa de diàleg

![fer visible](./deployments.images/deployments20.png)

Un cop "Visible" és seleccionat, el desplegament es tornarà verd

![desplegament verd](./deployments.ca.images/deployments27.png)

### Invisible

Per a fer-lo invisible, feu clic al botó ![botó ull obert](./deployments.ca.images/deployments26.png)

Apareixerà una caixa de diàleg

![fer invisible](./deployments.images/deployments23.png)

Hi ha dues opcions per triar:

- **No aturis els escriptoris**: Aquesta opció farà que el desplegament sigui invisible per a l'usuari. No obstant això, si un usuari està utilitzant actualment el seu escriptori en el moment en què es fa invisible, encara podran utilitzar-lo fins que decideixin aturar-lo. Un cop aturat, l'usuari no podrà accedir-hi de nou fins que el desplegament es torni a fer visible.

- **Atura els escriptoris**: Aquesta opció també farà invisible el desplegament. No obstant això, si qualsevol usuari ja està utilitzant el seu escriptori, li apagarà immediatament.

Un cop seleccionat "Invisible", el desplegament es tornarà vermell

![desplegament vermell](./deployments.ca.images/deployments24.png)


## Recrear els escriptoris

La funció "Recrear escriptoris" pretén modificar el desplegament afegint o eliminant escriptoris basats en els canvis fets als permisos de l'usuari. Si s'afegeix o elimina un usuari, aquesta funció actualitzarà el desplegament afegint o eliminant els seus escriptoris respectius .

Per fer-ho, feu clic al botó ![icona recrear](./deployments.ca.images/deployments31.png)

![notificació](./deployments.ca.images/deployments33.png)

## Com descarregar-se el fitxer de visors directes

Aquest fitxer s'utilitza per veure la llista d'usuaris al desplegament i els seus respectius URLs d'escriptori. Seguiu els passos següents per a descarregar aquest fitxer:

Primer feu clic al botó ![icona de descàrrega](./deployments.ca.images/deployments34.png)

Apareixerà una notificació:

![notificació](./deployments.ca.images/deployments35.png)

Heu de confirmar la notificació per baixar el fitxer CSV.

Aquest fitxer conté cada nom d'usuari, el seu nom, el seu correu electrònic i un enllaç que redirigeix a l'escriptori de l'usuari.

![contingut del fitxer csv](./deployments.ca.images/deployments36.png)

## Com esborrar un desplegament

Per a esborrar un desplegament, premeu el botó ![icona de la paperera](./deployments.ca.images/deployments37.png) a la llista de desplegament. També s'eliminaran tots els escriptoris de dins.

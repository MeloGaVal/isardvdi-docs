# Mitjans

En aquest apartat es veuran els mitjans creats per tu i els compartits amb tu.

Es prem el botó ![](./media.ca.images/mitjans1.png)

![](./media.ca.images/mitjans2.png)


## Els teus Mitjans

En aquest apartat es poden veure els mitjans que hagi pujat l'usuari.

![](./media.ca.images/mitjans3.png)


## Compartits amb tu

En aquest apartat es poden veure els mitjans que s'hagin compartit amb l'usuari.

![](./media.ca.images/mitjans4.png)


## Pujar Iso

Per a pujar una iso, es prem el botó ![](./media.ca.images/mitjans5.png)

![](./media.ca.images/mitjans6.png)

Redirigeix a una pàgina on s'ha d'emplenar els camps:

![](./media.ca.images/mitjans7.png)


## Compartir

Per a compartir una iso, es prem la icona ![](./media.ca.images/mitjans8.png)

I apareixerà una finestra de diàleg on poder seleccionar si es vol compartir per grups/usuaris

![](./media.ca.images/mitjans9.png)


## Generar escriptori a partir de mitjana

Per a generar un escriptori a partir d'una iso, es prem la icona ![](./media.ca.images/mitjans10.png)

![](./media.ca.images/mitjans11.png)

I et redirigeix a una pàgina on es pot emplenar el formulari amb el nom, visors, etc.

![](./media.ca.images/mitjans12.png)


## Eliminar una iso

Per a eliminar una iso, es prem el botó ![](./media.ca.images/mitjans13.png)

![](./media.ca.images/mitjans14.png)
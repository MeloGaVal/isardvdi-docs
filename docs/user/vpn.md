# VPN

In order to connect with VPN, you first have to select the "Wireguard" network of the desired desktop.

Press the icon ![](./vpn.images/vpn1.png)

![](./vpn.images/vpn2.png)

And the icon ![](./vpn.images/vpn3.png)

![](./vpn.images/vpn4.png)

In the "Hardware" section, in "Networks" select the network "Wireguard VPN"

![](./vpn.images/vpn5.png)

Go back to the Home screen and press the drop-down menu ![](./vpn.es.images/vpn9.png)

![](./vpn.images/vpn6.png)

And press the button ![](./vpn.images/vpn9.png)

![](./vpn.images/vpn7.png)

A file will be downloaded which should be given a short name.

![](./vpn.images/vpn8.png)

Open a terminal on your computer and run this command:

```
sudo cp ~/Downloads/user.conf /etc/wireguard/ # Downloads = the folder where the file was downloaded; user.conf = the name given to the vpn file

sudo wg-quick up user # user = name of the file

user:~$ ping (IP_imatge**)

user:~$ ssh isard@IP_imatge**
isard@ubuntu-server:~$

```

IP_imatge** : ![](./vpn.images/vpn10.png)
# Editar Escriptori

Es pot editar el nom de l'escriptori, la seva descripció, els visors que es vulguin utilitzar, deixar indicat l'usuari i contrasenya per a entrar amb rdp, canviar-li la configuració del maquinari, afegir-li una GPU si té, afegir un mitjana i canviar la imatge de l'escriptori.

Per a poder editar un escriptori es prem la icona ![](./edit_desktop.es.images/edit_desktop1.png)

![](./edit_desktop.ca.images/edit_desktop1.png)

I la icona ![](./edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.ca.images/edit_desktop2.png)

I redirigeix a una pàgina on poder editar.


## Visors

En aquest apartat es pot seleccionar què [visors](/user/viewers.ca/) es volen utilitzar per a visualitzar l'escriptori. També es pot seleccionar si es vol arrencar l'escriptori en pantalla completa o no.

![](./edit_desktop.ca.images/edit_desktop3.png)


## Login RDP

En aquest apartat s'afegeix l'usuari i contrasenya de login per al visor de RDP. Aquesta configuració només és necessària si no es vol estar identificant-se en l'escriptori per RDP tota l'estona i si el visor RDP està seleccionat.

![](./edit_desktop.ca.images/edit_desktop4.png)


## Hardware

En aquest apartat es pot editar el maquinari que es vulgui tenir en l'escriptori.

* Per a canviar la manera d'arrencada d'un escriptori, es canvia en la secció de "Boot". Si en la secció de "Mitjana" s'afegeix una iso, en "Boot" s'hauria de seleccionar "CD/DVD", sinó es deixa en "Hard Disk".

* Xarxes: Aquí es pot seleccionar la xarxa que es vulgui utilitzar, per defecte s'utilitza "Default" i si es vol utilitzar el visor RDP, s'ha de seleccionar a més la xarxa de "Wireguard VPN".

![](./edit_desktop.ca.images/edit_desktop5.png)


## Reservables

En aquest apartat es pot seleccionar la targeta de GPU que es vulgui associar a l'escriptori si és que té alguna disponible.

![](./edit_desktop.ca.images/edit_desktop6.png)


## Media

En aquest apartat se li pot afegir un iso que estigui compartida amb l'usuari.

![](./edit_desktop.ca.images/edit_desktop7.png)



## Imatge

En aquest apartat es pot seleccionar la imatge de portada que tindrà l'escriptori en la vista principal.

![](./edit_desktop.ca.images/edit_desktop8.png)
# Emmagatzematge

Des de la pàgina d'inici pots anar a la secció "Emmagatzematge" per a obtenir una mica més d'informació sobre els escriptoris que s'han creat com el consum dels escriptoris i plantilles.

![](./storage.ca.images/storage1.png)

![](./storage.ca.images/storage2.png)

En aquest apartat podem veure:

1. **ID**: És l'identificador de l'escriptori.
2. **Escriptoris/plantilles**: El nom que se li ha donat a l'escriptori/plantilla.
3. **Mida**: Mida ocupada que té l'escriptori en disc.

!!! Info Dependencies
    La mida proporcionada en aquesta secció no té en compte la mida del disc que es tria quan es crea l'escriptori. Comença a ocupar la mida del disc al moment de la creació.
4. **% Quota**: Indica el percentatge de disc que s'ha consumit respecte a la quota que té l'usuari.
5. **Últim accés**: Indica l'última data que es va accedir a l'escriptori o es va crear la plantilla.

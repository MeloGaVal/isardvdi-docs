# Perfil

Para ir al perfil, se pulsa el menú desplegable ![](./profile.es.images/profile1.png)

![](./profile.es.images/profile2.png)

Y el botón ![](./profile.es.images/profile4.png)

![](./profile.es.images/profile3.png)

## Cambiar de idioma

Para cambiar el idioma, se pulsa el menú desplegable y se escoge el idioma

![](./profile.es.images/profile5.png)

![](./profile.es.images/profile6.png)

## Cambiar contraseña

Para cambiar la contraseña se pulsa el botón ![](./profile.es.images/profile7.png)

![](./profile.es.images/profile8.png)

Y saldrá una ventana de diálogo dónde rellenar el formulario

![](./profile.es.images/profile9.png)

## Ver cuotas

Las cuotas definen la cantidad de recursos que se pueden utilizar basándose en el grupo al que pertenece el usuario.

Aquí se pueden ver todos los recursos que se haya utilizado.

![](./profile.es.images/profile10.png)
# Visors

En VDI (Virtual Desktop Infrastructure), els visors són importants perquè permeten l'accés remot als escriptoris virtuals. VDI permet als usuaris accedir a un escriptori virtual des de qualsevol dispositiu amb una connexió a Internet, i els visors proporcionen la interfície perquè l'usuari interactuï amb aquest escriptori virtual.

Els visors solen proporcionar funcions com ara protocols de visualització, compressió, redirecció multimèdia, redirecció de dispositius USB i autenticació. Permeten als usuaris accedir a l'entorn d'escriptori virtual, utilitzar aplicacions i accedir a dades des d'una ubicació remota com si estiguessin físicament asseguts a l'escriptori.

Els visors també juguen un paper crític en assegurar que l'experiència d'escriptori virtual sigui el més propera possible a l'experiència d'escriptori local. Han de ser capaços de manejar gràfics d'alta resolució, contingut multimèdia i altres tipus de dades sense causar retards o interrupcions en l'experiència de l'usuari.

En resum, els visors són essencials en VDI perquè proporcionen els mitjans perquè els usuaris accedeixin i interactuïn amb els escriptoris virtuals, i juguen un paper crític per assegurar que l'experiència d'escriptori virtual sigui tan fluida i eficient com sigui possible.

Tenim aquests clients de visors per a connectar-se als escriptoris:

- [**Visor SPICE**](#spice-viewer): es necessita instal·lar l'aplicació client del visualitzador remot.
- [**Visor al navegador**](#browser-viewer): funciona amb navegadors moderns
- [**RDP**](#rdp): es necessita instal·lar l'aplicació client RDP
- [**Navegador RDP**](#rdp-browser): funciona amb navegadors moderns
- [**RDP VPN**](#rdp-vpn): es necessita instal·lar l'aplicació client RDP i la connexió client VPN IsardVDI

També podeu activar l'opció **visor directe** que us portarà un enllaç directe per a connectar-vos a l'escriptori sense la necessitat d'autenticar-vos al sistema.

## Taula resum de característiques dels visors

| Visor IsardVDI:      | Visor SPICE                                                  | Visor al navegador | RDP | Navegador RDP | RDP VPN |
|--------------------------|-------------------|------------------------|-----------------|-------------------------------|--------------------------|
| **Protocol** | SPICE    | VNC-SPICE       | RDP                  | RDP           | RDP                    |
| **HTML5 web client**        |  -                   | NoVNC              | -                    | Guacamole  | -            |
| **Visor windows**  | Remote-viewer <br />([instal·lador virt-viewer 11](https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi)) | Navegador web | Visor Escriptori Remot (windows) | Navegador web | -       |
| **Visor linux** | sudo apt install virt-viewer  <br /> sudo dnf install remote-viewer | Navegador web | Remmina | Navegador web | Remmina |
| **Visor mac** | Podeu seguir la [guia d'instal·lació](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c) | Navegador web | [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12) | Navegador web | [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12) |
| **Visor Android** | Hi ha una [versió lliure](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) a la Play Store i també una [de pagament](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) amb més característiques. | Navegador web | [escriptori remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) | Navegador web | [escriptori remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) |
| **Visor Apple iOS** | [versió de pagament](https://apps.apple.com/gb/app/aspice-pro/id1560593107) a Apple Store |  | [escriptori remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) |  | [escriptori remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) |
| **Extensió fitxer descarregat**                            | *.vv                                                         | Navegador web | *.rdp                |              | *.rdp   |
| **Copy / Paste escriptori real a escriptori virtual** | SI | NO              | SI                   | SI         | SI |
| **Cal client wireguard per accedir VPN d'usuari** | NO | NO | NO | NO | SI |
| **Redirecció USB** | SI<br />(amb windows cal instal·lar usb) | NO | SI | NO | SI |
| **Audio** | SI | NO | SI | NO | SI |
| **Adaptar tamany pantalla d'escriptori al visor** | SI | SI | SI | SI | SI |
| **Pots veure com arrenca l'escriptori** | SI | SI | NO | NO | NO |
| **Cal tenir adreça IP i servei RDP a l'escriptori vitual**| NO | NO | SI | SI | SI |


| Visor VDI               | SO Compatible                      | Protocols | Redirecció multimèdia | Redirecció USB | Compressió de Vídeo             |
|--------------------------|-----------------------------------|-------------------|------------------------|-----------------|-------------------------------|
| Microsoft Remote Desktop | Windows, Mac, iOS, Android        | RDP               | :material-check:                    | :material-check:             | RemoteFX                      |
| Spice                    | Windows, Linux                    | SPICE             | :material-check:                    | :material-check:             | Lossless i Lossy            |
| VNC                      | Windows, Mac, Linux, iOS, Android | VNC               | :material-close:                     | :material-check:             | Tight i Zlib                |
| Guacamole HTML5 Client   | Windows, Mac, Linux, iOS, Android | RDP, VNC, SSH     | :material-check:                    | :material-check:             | JPEG, PNG                     |
| noVNC Client             | Windows, Mac, Linux, iOS, Android | VNC               | :material-check:                    | :material-check:             | Tight, Hextile, CopyRect, Raw |


| Visor VDI               | Característiques                                                                                                                                   | Avantatges                                                                                                                                                                          | Inconvenients                                                                                                                       |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| Microsoft Remote Desktop | - Protocol RDP <br> - Redirecció multimèdia <br> - Redirecció USB <br>- Compressió de vídeo RemoteFX                          | - Suport integrat en els sistemes operatius Windows <br>- RemoteFX proporciona transmissió de vídeo i àudio d'alta qualitat <br>- Es pot configurar i utilitzar fàcilment                         | - Suport limitat per a sistemes operatius no Windows <br>- Seguretat relacionada amb el protocol RDP                        |
| Spice                    | - Protocol SPICE <br>- Redirecció multimèdia <br>- Redirecció USB <br>- Compressió de vídeo sense pèrdua i amb pèrdua              | - Programari de codi obert amb comunitat de desenvolupament actiu <br>- Permet múltiples sistemes operatius <br>- Optimitzat per a rendiment alt i baixa latència             | - Suport multimèdia limitat en comparació amb altres protocols <br>- Redirecció USB limitada en comparació amb altres protocols   |
| VNC                      | - Protocol VNC <br>- Redirecció USB <br>- Compressió de vídeo Tight i Zlib                                                      | - Programari de codi obert amb comunitat de desenvolupament actiu <br>- Permet múltiples sistemes operatius <br>- Es pot configurar i utilitzar fàcilment                                     | - Suport multimèdia limitat en comparació amb altres protocols <br>- Característiques de seguretat limitades en comparació amb altres protocols |
| Client Guacamole HTML5   | - Protocols RDP, VNC, i SSH  <br>- Redirecció multimèdia <br>- Redirecció USB <br>- Compressió de vídeo JPEG i PNG       | - Accessible des de qualsevol navegador web compatible amb HTML5 <br>- Permet múltiples protocols i sistemes operatius <br>- No cal instal·lar el programari client al dispositiu local | - Requereix la instal·lació i configuració del programari del servidor <br>- Rendiment limitat en comparació amb els clients nadius   |
| noVNC Client             | - VNC protocol <br>- Redirecció multimèdia <br>- Redirecció USB <br>- Compressió de vídeo Tight, Hextile, CopyRect, i Raw  | - Accessible des de qualsevol navegador web compatible amb HTML5 <br>- Permet múltiples sistemes operatius <br>- Permet múltiples formats de compressió de vídeo                     | - Suport de protocol limitat en comparació amb altres visualitzadors <br>- Suport multimèdia limitat en comparació amb altres protocols    |


## Visor SPICE

![escriptori](viewers.ca.images/visor10.png)

- Cal tenir instal·lada l'aplicació client:

    - *Windows*: [virt-viewer](https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi)
    - *Apple*: Podeu seguir la [guia d'instal·lació](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)
    - *Linux*: `sudo apt install virt-viewer` / `sudo dnf install remote-viewer`
    - *Android*: Hi ha una [versió lliure](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) a la Play Store i també una [de pagament](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) amb més característiques.
    - *Apple iOS*: [versió de pagament](https://apps.apple.com/gb/app/aspice-pro/id1560593107) a Apple Store

- Latència baixa
- Àudio integrat
- [Fitxer adjunt](#plug-devices-with-spice) opcional d'USB 

# Visor al navegador

![](viewers.ca.images/visor1.png)

- Funciona amb qualsevol navegador modern
- Latència mitjana
- No hi ha àudio disponible
- No hi ha cap fitxer adjunt de dispositius locals disponible


## RDP

![](viewers.images/viewers_rdp.png)

!!! Info Dependencies
    El visor només està disponible quan s'ha iniciat l'escriptori i s'ha obtingut l'adreça IP

- Cal tenir instal·lada l'aplicació client (ja instal·lada al Windows)

    - *Windows*: [Clients d'escriptori remots](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)
    - *Mac OSX*: [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)
    - *Linux*: Remmina
    - *Android*: [escriptori remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx)
    - *Apple iOS*: [escriptori remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092)

- Latència baixa (millor amb Windows)
- Àudio integrat
- [Fitxer adjunt](#plug-devices-with-rdp) opcional d'USB
- Millor quan s'utilitzen aplicacions 3D al Windows
- Els escriptoris GPU només funcionaran amb clients RDP.

Per a totes les connexions RDP, l'escriptori hauria d'estar [configurat](#activate-rdp)

## Navegador RDP

!!! Info Dependencies
    El visor només està disponible quan s'ha iniciat l'escriptori i s'ha obtingut l'adreça IP

- Funciona amb qualsevol navegador modern
- Latència baixa
- Àudio integrat
- No hi ha cap fitxer adjunt de dispositius locals disponible
- Els escriptoris GPU només funcionaran amb clients RDP.

## RDP VPN

!!! Info Dependencies
    El visor només està disponible quan s'ha iniciat l'escriptori i s'ha obtingut l'adreça IP

- Cal tenir instal·lada l'aplicació client (ja instal·lada al Windows)

    - *Windows*: [Clients d'escriptori remots](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)
    - *Mac OSX*: [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)
    - *Linux*: Remmina
    - *Android*: [escriptori remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx)
    - *Apple iOS*: [escriptori remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092)

- Necessita tenir la configuració i la connexió de la [ VPN IsardVDI](vpn.md)
- Disponible quan s'ha iniciat l'escriptori i obté una adreça IP
- Latència baixa (millor amb Windows)
- Àudio integrat
- [Fitxer adjunt](#plug-devices-with-rdp) opcional d'USB
- Millor quan s'utilitzen aplicacions 3D al Windows
- Els escriptoris GPU només funcionaran amb clients RDP.

# Activa el RDP

## Escriptoris Windows

Per permetre connexions RDP al vostre escriptori, cal activar RDP a Windows i desactivar l'autenticació de xarxa en opcions avançades al cguest de Windows.

1. Activa el RDP

![](./viewers.images/guest_rdp_cfg1.png)

2. Desactiva l'autenticació de xarxa

![](./viewers.images/guest_rdp_cfg2.png)

## Escriptoris Linux

- https://nextcloud.isardvdi.com/s/XY4ow9P5NHASxmx

# Connecta dispositius USB

## Connectors amb RDP

Des de l'escriptori on voleu connectar l'USB, premeu el botó ![](./viewers.es.images/visor21.png)

![](./viewers.ca.images/visor20.png)

Heu de descarregar el fitxer de visualització que voleu utilitzar amb rdp

![](./viewers.ca.images/visor21.png)

Obriu el fitxer on teniu la descàrrega i premeu al botó «modifica»

![](./viewers.es.images/visor24.png)

Premeu el botó «Recursos locals»

![](./viewers.es.images/visor25.png)

I premeu el botó "Més" als dispositius i recursos locals

![](./viewers.es.images/visor26.png)

Un cop allà, a la secció "Units" seleccionem l'USB que volem, en el meu cas he seleccionat el "SUN-USB(D:)"

![](./viewers.es.images/visor27.png)

Es prem el botó «Connecta»

![](./viewers.es.images/visor28.png)

![](./viewers.es.images/visor29.png)

Usuari: isard and password: pirineus -> per defecte

![](./viewers.es.images/visor30.png)

Premeu el botó "sí"

![](./viewers.es.images/visor31.png)

I quan entreu a través de l'escriptori remot, hauria de sortir alguna cosa així. En aquest cas és D perquè és on era al meu ordinador.

![](./viewers.es.images/visor32.png)

## Connectors amb Spice

!!! Info Dependencies
    Això s'aplica només al Windows, ja que al client de visor remot linux aquesta opció està activa de manera predeterminada.

!!! Important Dependencies
    Descarregeu el controlador USB perquè es pugui detectar al visor. (UsbDk):1.0.22x64.msi): https://www.spice-space.org/download.html

Un cop el controlador s'hagi descarregat, obriu el visor Spice, premeu el botó «Fitxer»

![](./viewers.es.images/visor33.png)

I l'opció "Selecció de dispositiu USB" està seleccionada

![](./viewers.es.images/visor34.png)

I el dispositiu que voleu utilitzar està seleccionat.

![](./viewers.es.images/visor35.png)

<hr>

# Visualitzadors i detalls tècnics

Podem diferenciar dos tipus principals d'espectadors depenent del lloc on accedim:

* **Visors integrats en el navegador**: el visor està integrat en una pàgina web, des d'una pestanya del navegador podem gestionar una finestra o un linux. La descodificació del senyal de vídeo i l'enviament del senyal del ratolí i del teclat es fa des d'una pestanya del navegador.

    * **Advantatge**:
        * No cal tenir instal·lat un client, funciona des de qualsevol dispositiu (ordinadors, tauletes, telèfons mòbils) i sistema operatiu que tingui un navegador.

    * **Disavantatges**:
        * La descodificació no és tan eficient com en un visor dedicat, notablement més **lent** en actualitzar l'escriptori i en moure elements al voltant de l'escriptori.
        * No podem redirigir als dispositius locals de l'escriptori virtual connectats per USB com ara un llapis, una càmera web...

* **Aplicacions de client de l'escriptori**: són aplicacions dedicades a actuar com a visors, estan optimitzades i són la millor opció per a tenir la millor experiència d'usuari.

    * **Advantatges**:

      * Optimitzat per a protocols de client d'escriptori, millor rendiment que els visualitzadors en el navegador. Si no hi ha problemes de xarxa (latència i amplada de banda adequades) la sensació de treballar amb l'escriptori és similar a la d'un escriptori real.

       * Tot i que depèn del tipus de protocol i de la versió del client, en general ofereixen opcions avançades com ara: redirecció de ports USB, còpia i enganxa des de l'escriptori virtual a l'escriptori real, carregar fitxers a través del visor a l'escriptori virtual...

    * **Disavantatges**:
       * En tots els casos no venen preinstal·lats amb el sistema operatiu, per la qual cosa és necessari dur a terme una instal·lació en el sistema operatiu de l'ordinador des del qual ens connectem.

       * Les característiques avançades no estan disponibles depenent de les versions del client i la versió del protocol

## Protocols i visors disponibles a Isard

* **SPICE**: és un protocol de comunicació per als entorns virtuals. Ens permet accedir al senyal de vídeo, ratolí i teclat de l'escriptori com si estiguéssim connectats a la pantalla, el ratolí i el teclat d'un ordinador real.

    * **Advantatges**:

        * És el motor de màquina virtual que Isard utilitza (Qemu-KVM) el que ens dóna accés independentment del sistema operatiu que s'estigui executant.
        * Podem utilitzar aquest protocol per a veure tota la seqüència d'arrencada de l'escriptori, instal·lar sistemes operatius...
        * No necessitem instal·lar cap component en el sistema operatiu d'escriptori virtual per a poder interactuar amb ell.
        * És un protocol que optimitza l'amplada de banda de vídeo utilitzat en comprimir el senyal i enviar només les àrees que varien d'un fotograma a un altre.

    * **Disavantatges**:
        * El client no està instal·lat per defecte en cap sistema operatiu, la instal·lació és molt senzilla i pot ser realitzada per qualsevol usuari, però en entorns corporatius o educatius les restriccions de permisos poden dificultar la instal·lació.
        * La instal·lació al Windows requereix un programa addicional per a poder redirigir els ports USB
        * La connexió es fa a través d'un servidor intermediari HTTP mitjançant un mètode "CONNECT". Aquest mètode, en alguns casos, és filtrat per algun tallafoc o intermediari.

* **VNC**: és un protocol que funciona al mateix nivell que SPICE, però com que és més antic, el senyal de vídeo no està tan optimitzat. És el protocol que utilitzem al client integrat al navegador

* **RDP**: és el protocol utilitzat de manera predeterminada per a connectar remotament a un sistema operatiu Windows. Necessita que permetem l'accés remot al sistema operatiu. No està disponible des de l'arrencada de l'escriptori i heu d'esperar que la seqüència d'arrencada de l'escriptori obtingui una adreça IP. El principal avantatge és que ofereix la millor experiència suau per a un escriptori Windows, utilitzant el client RDP natiu actual. És el protocol recomanat també quan s'utilitza conjuntament amb les targetes vGPU Nvidia.

    * **Advantatges**:
        * La millor experiència d'usuari si el sistema operatiu de l'escriptori virtual és Windows i l'ordinador des del qual es connecta el client també és Windows
        * Als clients de Windows no és necessari instal·lar programari addicional, ja que el client d'escriptori remot ve per defecte a totes les finestres
        * És necessari per a una bona experiència d'usuari utilitzant VGPUs Nvidia en sistemes operatius Windows.

    * **Disavantatges**:
        * Si hi ha un problema en iniciar el sistema operatiu, no accediu al senyal de pantalla (el resoleu connectant a través de spice o vnc)

## Ports i servidors intermediaris

Isard ha fet un esforç significatiu per evitar haver d'obrir ports addicionals i encapsular connexions en servidors intermediaris HTTP. Es tracta d'adaptar-se a qualsevol situació on hi hagi un tallafoc que pugui dificultar la connexió. Per defecte s'utilitzen els ports:

- TCP/80 per al servidor intermediari on es troben les connexions del protocol SPICE
- TCP/443 per a visors web i incrustats en navegadors
- TCP/9999 per al servidor intermediari a través del qual s'encapsulen les connexions del protocol RDP

## Visor VNC al navegador

Per a poder veure un escriptori directament des del navegador, seleccioneu l'opció ![](./viewers.ca.images/visor1.png)

![](./viewers.ca.images/visor2.png)

!!! Important Dependencies
    Cal tenir en compte si el navegador està bloquejat per a obrir finestres emergents

Al Firefox: apareixerà un missatge sobre finestres emergents

![](./viewers.ca.images/visor3.png)

Premeu el botó ![](./viewers.ca.images/visor4.png)

![](./viewers.ca.images/visor5.png)

I obrirà l'escriptori en una pestanya nova del navegador

![](./viewers.ca.images/visor17.png)

## Visor per al protocol SPICE

!!! Important Dependencies
Es recomana utilitzar la [versió 7](https://www.spice-space.org/download.html) de Spice.*

Per a poder utilitzar el visualitzador Spice, baixeu el client visualitzador:

### Microsoft
#### Aplicació client Spice

* [Client de visor Spice](https://releases.pagure.org/virt-viewer/virt-viewer-x64-9.0.msi)
* [Client USB del visor per al Windows](https://www.spice-space.org/download/windows/usbdk/UsbDk)1.0.22)x64.msi)

### Linux

#### Aplicació client Spice

* Distros basades en Debian/Ubuntu: `sudo apt install virt-viewer -y`
* Distros basades en RedHat/CentOS/Fedora: `sudo dnf install remote-viewer -y`

Un cop instal·lat, en arrencar un escriptori podreu seleccionar l'opció «Visor de la selecció» des del menú desplegable

![](./viewers.ca.images/visor10.png)

Apareixerà un diàleg perquè pugueu obrir el fitxer Spice

![](./viewers.ca.images/visor11.png)

Un cop obert, apareixerà una finestra amb l'escriptori

![](./viewers.ca.images/visor12.png)

## Visor RDP

!!! Important Dependencies
    Perquè el client RDP connecti **Cal tenir la xarxa Wireguard activada a l'escriptori a través de la qual connectarem i tenir l'"escriptori Remote" configurat a l'escriptori**

En els ordinadors amb el sistema operatiu Windows no és necessari instal·lar cap programari addicional, ja que tenen el client d'escriptori remot instal·lat com una eina de windows.

### Aplicació client RDP (Windows)

* Normalment, Windows ja té instal·lada la `Connexió d'escriptori Remot`. Si no, consulteu a [clients d'escriptori Remote](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)

En els ordinadors amb Linux heu d'instal·lar un client com "Remmina" i en macOS hi ha una aplicació oficial, "Microsoft Remote Desktop", que es pot descarregar des de la [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### Aplicació client RDP (Linux)

* A Linux es distribueix l'aplicació client per a RDP que s'anomena `Remmina` i es pot instal·lar seguint [aquesta guia](https://remmina.org/how-to-install-remmina/)

Com ja s'ha esmentat, el visor RDP necessita una IP per a connectar-se, de manera que quan s'inicia un escriptori, veiem un parpelleig i els enllaços als visors RDP apareixen com a no seleccionables:

![](./viewers.ca.images/visor16.png)

Un cop obtinguda l'adreça IP, s'informa i s'activa l'accés als espectadors:

![](./viewers.ca.images/visor13.png)

Seleccionant el *visor RDP* es baixa un fitxer amb una extensió .rdp:

![](./viewers.ca.images/visor14.png)

El fitxer isard-rdp-gw.rdp conté la informació per a poder connectar amb l'escriptori.

!!! Info Dependencies
    És probable que la primera vegada en els sistemes operatius Windows ens informi amb una alerta de seguretat, podem confirmar-ho a la caixa "No em tornis a preguntar sobre les connexions a aquest ordinador" i en el futur no ens tornarà a preguntar.

![](viewers.es.images/visor15.png)

Ara ens demana les credencials. És important tenir en compte que aquest nom d'usuari i contrasenya és el de l'escriptori virtual que s'està executant. A les plantilles que oferim per defecte:

* l'usuari és: isard
* la contrasenya és: pirineus

![](viewers.es.images/visor18.png)

En accedir a l'escriptori, ens demana confirmació per acceptar el certificat:

![](viewers.es.images/visor19.png)

I finalment el client s'obre i podem interactuar amb l'escriptori.

## Descarrega els visors de clients nadius per a altres distribucions

Per a l'aplicació client spice trobareu més informació per a qualsevol SO a [spice-space](https://www.spice-space.org/download.html)*

### MacOS

#### Aplicació client Spice

* Podeu seguir la [guia d'instal·lació](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)

#### Aplicació client RDP

* Hi ha una aplicació oficial, `Microsoft Remote Desktop`, que es pot descarregar des de la [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### Android

#### Aplicació client Spice

* Hi ha una [versió lliure](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) a la Play Store i també una [de pagament](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) amb més característiques.

#### Aplicació client RDP

* Microsoft té el seu propi [Escriptori Remot](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) a la Play Store

### IOS

#### Aplicació client Spice

* Hi ha una [versió de pagament](https://apps.apple.com/gb/app/aspice-pro/id1560593107) a Apple Store

#### Aplicació client RDP

* Microsoft té el seu propi [Escriptori Remot](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) a Apple Store

### Visor RDP

Podeu activar múltiples connexions RDP en crear o editar un escriptori:

![](./viewers.ca.images/visor22.png)

- RDP web (HTML5)
- RDP (client natiu a través del servidor intermediari IsardVDI al port predeterminat tcp/9999)
- RDP vpn (a través de wireguard vpn)

!!! Important Dependencies
    Tots els visors necessiten que l'escriptori tingui la interfície `Wireguard VPN` juntament amb qualesvol altres.

#### RDP HTML5

IsardVDI utilitza el servidor guacamole que permet als clients RDP HTML5 (qualsevol navegador real) connectar-se a les finestres IsardVDI a través del port https predeterminat. També té àudio a través del navegador.

#### RDP Natiu 

Només necessitareu el client (client RDP a Windows o Remmina a Linux) i que l'escriptori tingui **la interfície `Wireguard` afegida.**

Aquesta connexió es realitza de manera predeterminada a través del port tcp/9999, però si cal, podreu canviar-la a `isardvdi.cfg`.

#### RDP VPN

També necessitareu afegir el client i **la interfície `Wireguard`** a l'escriptori.

Per a connectar a través de wireguard vpn a RDP, haureu de configurar el vostre fitxer de client de wireguard `isard-vpn.conf` a l'ordinador del client. Consulteu ![utilitzant vpn](https://isard.gitlab.io/isardvdi-docs/user/vpn/) a IsardVDI.

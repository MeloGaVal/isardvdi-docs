# Reserves

En aquest apartat veurem com fer reserves per als escriptoris amb tecnologia vGPU associades a recursos GPUs de targetes Nvidia, com afegir aquest tipus de maquinari als escriptoris virtuals i la gestió de les reserves associada.

Per més informació, consultar l'[apartat de GPU](../deploy/gpus/gpus.ca.md).


## Per què un sistema de reserves

IsardVDI està pensat perquè els usuaris de forma autònoma puguin crear i desplegar escriptoris, el límit de quant escriptoris poden córrer de forma simultània en el sistema ve condicionat pels recursos disponibles. Si els escriptoris no tenen vGPUs, el límit sol venir per la quantitat de memòria RAM disponible en els hipervisors, essent aconsellable que sempre la capacitat de memòria i vCPUs d'aquests servidors sigui suficient per sostenir la concorrència d'escriptoris. Podem limitar la quantitat de recursos que atorguem a cada usuari, grup o categoria amb les quotes i límits que IsardVDI permet establir.

En el cas de les vGPUs, són recursos cars (hi ha el cost de la targeta i el cost del llicenciament) i per accedir a aquests recursos s'ha establert un sistema de reserves, amb les següents característiques:

- L'administrador pot **planificar horaris amb diferents perfils** aplicats per a cada targeta disponible, permetent que en un determinat horari, s'estableixi un perfil amb molta memòria dedicada i pocs usuaris (p. ex.: llançar una renderització), i en un altre horari, un altre perfil amb poca memòria i molts usuaris (p. ex.: realitzar una formació).

- Existeix un **sistema de permisos** granular que permet definir quins usuaris tenen permís per usar un determinat perfil d'una targeta. Permet que només un grup reduït d'usuaris pugui accedir als perfils amb més memòria.

- Un usuari pot reservar un escriptori per a una determinada franja horària amb un **temps mínim previ** a la reserva i una **durada màxima de la reserva**. Permet que es puguin fer reserves amb poc temps d'antelació i que no es puguin reservar franges superiors a un nombre reduït d'hores.

- Un usuari avançat pot fer una reserva **per a un desplegament**, reservant tantes unitats de perfils de GPU com escriptoris conté el desplegament.

- Existeix també un sistema de **prioritats de reserves**, que permet posar regles perquè uns usuaris puguin sobreescriure i revocar reserves d'altres. Aquesta opció és útil si volem fomentar que la targeta GPU s'utilitzi per molts usuaris, però volem alhora garantir que per a uns determinats grups mai els falti capacitat de reserves, com per exemple, si un grup


## Preparar escriptori/desplegament

### Crear/Editar un escriptori/desplegament perquè funcioni amb GPU

[Crear](create_desktop.ca.md)/[Editar escriptori](edit_desktop.ca.md) o [crear nou desplegament](../advanced/deployments.ca.md) i desplaçar-se fins a arribar a l'apartat **Hardware**.

És important que la configuració **Vídeos** tingui l'opció **Only GPU** marcada. En els escriptoris amb S.O. Windows amb els drivers de Nvidia instal·lats, no és compatible tenir alhora una targeta de vídeo QXL i una targeta vGPU.

![](bookings.images/hardware_videos_only_gpu.png)

En l'apartat següent, **Reservables**, més avall, s'assigna el **perfil** de GPU desitjat.

![](bookings.images/bookables_gpu_dropdown.png)

NOTA: *Per poder reservar i iniciar un escriptori amb un perfil, l'administrador ha de planificar-ho prèviament.*


### Només visors RDP

Els escriptoris amb GPU fan ús del controlador de targeta GPU i per tant **només és possible connectar-se a ells mitjançant els visors RDP**.

![](bookings.images/rdp_viewers.png)

**IMPORTANT!** Si hi ha un problema amb el sistema operatiu i mai arribem a tenir adreça IP, no podrem accedir per RDP. Sempre cal evitar això, però, amb més raó en aquest apartat, forçar l'apagat de l'escriptori des de la interfície d'IsardVDI, perquè pot provocar una corrupció del disc (ja que és l'equivalent a treure el cable de corrent d'un ordinador encès) i que aquest no aconsegueixi iniciar el sistema operatiu i obtenir adreça IP, amb la qual cosa ens podrem quedar sense accés a l'escriptori.


## Reservar un escriptori/desplegament

És necessari realitzar les **reserves** dels **escriptoris** amb vGPU. L'administrador haurà planificat uns perfils de GPU en les diferents targetes disponibles i l'usuari podrà fer reserves sempre que tingui permisos i coincideixi el perfil del seu vGPU amb una planificació disponible.

S'accedeix a reservar una GPU per l'escriptori des de la última icona d'accions d'aquest.

Per a reservar un desplegament, s'accedeix mitjançant el panell de desplegaments, amb el mateix botó, que es troba a la dreta del desplegament.

![](bookings.images/desktop_card_action_schedule.png)

![](bookings.images/deployments_panel_schedule.png)

Ens dóna accés a la vista setmanal de la **disponibilitat** (podem canviar la vista a mensual o diària), on trobem dues columnes per cada dia de la setmana. En la columna de l'esquerra surt la disponibilitat pel perfil de targeta, i en la columna de la dreta és on sortiran les reserves que tenim per a aquest escriptori o desplegament.

En la figura s'aprecia que existeix **disponibilitat** durant la setmana i no hi ha feta **cap** reserva.

![](bookings.images/booking_panel_availability.png)

Les reserves poden crear-se mitjançant el botó de dalt a la dreta ![](bookings.images/add_booking_button.png) o mitjançant el cursor, **clicant en la franja Reserves i arrossegant** per a seleccionar el rang d'hora desitjat. En el formulari que apareixerà podrem ajustar el rang de dates i hores de durada de la reserva que volem realitzar.

![](bookings.images/add_booking_modal.png)

Una vegada feta la reserva, sortirà a la columna de la dreta de cada jornada. 

![](bookings.images/booking_1_drag_panel.png)


## Editar o esborrar reserva

Per a editar una reserva, es fa clic sobre ella i poder així modificar els rangs de temps de durada de dita reserva. No es pot modificar una reserva que està en curs.
Per a eliminar una reserva, es fa clic sobre la franja i mitjançant el botó d'eliminació que surt al editar-la.

![](./bookings.images/edit_and_delete_booking_modal.png)


## Videotutorials

Aquí oferim dos videotutorials que expliquen les configuracions prèvies d'un escriptori o desplegament, i com realitzar reserves per ambdues opcions, amb els **dos mètodes de reserva que existeixen a IsardVDI**, a la primera part i segona part respectivament. 

Subtítols en català disponibles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
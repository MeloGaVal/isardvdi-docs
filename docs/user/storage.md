# Storage

From the home page you can go to the "Storage" section to get a little more information about the desktops that have been created like the consumption of the desktops and templates.

![](./storage.images/storage1.png)

![](./storage.images/storage2.png)

In this section we can see:

1. **ID**: It is the identifier of the desktop.
2. **Desktops/templates**: The name given to the desktop/template.
3. **Size**: Occupied size of the desktop on disk.

!!! Info Dependencies
    The size provided in this section does not take into account the disk size that is chosen when the desktop is created. It begins to occupy the size of the disk at the time of creation.

4. **% Quota**: Indicates the percentage of disk that has been consumed in respect to the quota that the user has.
5. **Last Access**: Indicates the last date the desktop was accessed or the template was created.
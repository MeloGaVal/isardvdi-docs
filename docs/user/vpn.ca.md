# VPN

Per a poder connectar-se amb VPN, primer s'ha de seleccionar la xarxa "Wireguard" de l'escriptori que es vulgui.

Es prem la icona ![](./vpn.images/vpn1.png)

![](./vpn.ca.images/vpn1_ca.png)

I la icona ![](./vpn.images/vpn3.png)

![](./vpn.ca.images/vpn4_ca.png)

En l'apartat de "Hardware", en "Xarxes" se selecciona la xarxa "Wireguard VPN"

![](./vpn.ca.images/vpn5_ca.png)

Es torna a la pantalla d'Inici i es prem el menú desplegable ![](./vpn.es.images/vpn9.png)

![](./vpn.ca.images/vpn6_ca.png)

I es prem el botó ![](./vpn.images/vpn9.png)

![](./vpn.ca.images/vpn8_ca.png)

Es descarregarà un arxiu al qual s'hauria d'assignar un nom curt.

![](./vpn.ca.images/vpn9_ca.png)

S'obre un terminal en la teva ordinador i s'executa aquest comando:

```
sudo cp ~/Downloads/user.conf /etc/wireguard/ # Downloads = la carpeta on s'hagi descarregat l'arxiu; user.conf = el nom que se li ha donat a l'arxiu vpn

sudo wg-quick up user # user = nom de l'arxiu anterior

user:~$ ping (IP_imatge**)

user:~$ ssh isard@IP_imatge**
isard@ubuntu-server:~$

```

IP_imatge** : ![](./vpn.ca.images/vpn7_ca.png)
# Perfil

Per a anar al perfil, es prem el menú desplegable ![](./profile.es.images/profile1.png)

![](./profile.ca.images/profile1.png)

I el botó ![](./profile.es.images/profile4.png)

![](./profile.ca.images/profile2.png)

## Canviar d'idioma

Per a canviar l'idioma, es prem el menú desplegable i es tria l'idioma

![](./profile.ca.images/profile4.png)

![](./profile.ca.images/profile3.png)

## Canviar contrasenya

Per a canviar la contrasenya es prem el botó ![](./profile.ca.images/profile5.png)

![](./profile.ca.images/profile6.png)

I sortirà una finestra de diàleg on emplenar el formulari

![](./profile.ca.images/profile7.png)

## Veure quotes

Les quotes defineixen la quantitat de recursos que es poden utilitzar basant-se en el grup al qual pertany l'usuari.

Aquí es poden veure tots els recursos que s'hagi utilitzat.

![](./profile.ca.images/profile8.png)
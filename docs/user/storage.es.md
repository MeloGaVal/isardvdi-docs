# Almacenamiento

Desde la página de inicio puedes ir a la sección "Almacenamiento" para obtener un poco más de información sobre los escritorios que se han creado como el consumo de los escritorios y plantillas.

![](./storage.es.images/storage1.png)

![](./storage.es.images/storage2.png)

En este apartado podemos ver:

1. **ID**: Es el identificador del escritorio.
2. **Escritorios/plantillas**: El nombre que se le ha dado al escritorio/plantilla.
3. **Tamaño**: Tamaño ocupado que tiene el escritorio en disco.

!!! Info Dependencies
    El tamaño proporcionado en esta sección no tiene en cuenta el tamaño del disco que se escoge cuando se crea el escritorio. Comienza a ocupar el tamaño del disco al momento de la creación.
4. **% Cuota**: Indica el porcentaje de disco que se ha consumido respecto a la cuota que tiene el usuario.
5. **Último acceso**: Indica la última fecha que se accedió al escritorio o se creó la plantilla.
# Viewers

In VDI (Virtual Desktop Infrastructure), viewers are important because they enable remote access to virtual desktops. VDI allows users to access a virtual desktop from any device with an internet connection, and viewers provide the interface for the user to interact with that virtual desktop.

Viewers typically provide features such as display protocols, compression, multimedia redirection, USB device redirection, and authentication. They allow users to access the virtual desktop environment, use applications, and access data from a remote location as if they were physically sitting at the desktop.

Viewers also play a critical role in ensuring that the virtual desktop experience is as close to the local desktop experience as possible. They need to be able to handle high-resolution graphics, multimedia content, and other types of data without causing delays or disruptions in the user's experience.

In summary, viewers are essential in VDI because they provide the means for users to access and interact with virtual desktops, and they play a critical role in ensuring that the virtual desktop experience is as seamless and efficient as possible.

We've got these client viewers to connect to desktops:

- [**SPICE viewer**](#spice-viewer): needs remote-viewer client app installed.
- [**Browser viewer**](#browser-viewer): works with modern browsers
- [**RDP**](#rdp): needs RDP client app installed
- [**RDP Browser**](#rdp-browser): works with modern browsers
- [**RDP VPN**](#rdp-vpn): needs RDP client app installed and IsardVDI VPN client connection

You can also activate the **direct viewer** option that will bring you a direct link to connect to your desktop without the need to authenticate into the system.

| VDI Viewer               | Supported OS                      | Protocols | Multimedia Redirection | USB Redirection | Video Compression             |
|--------------------------|-----------------------------------|-------------------|------------------------|-----------------|-------------------------------|
| Microsoft Remote Desktop | Windows, Mac, iOS, Android        | RDP               | :material-check:                    | :material-check:             | RemoteFX                      |
| Spice                    | Windows, Linux                    | SPICE             | :material-check:                    | :material-check:             | Lossless and Lossy            |
| VNC                      | Windows, Mac, Linux, iOS, Android | VNC               | :material-close:                     | :material-check:             | Tight and Zlib                |
| Guacamole HTML5 Client   | Windows, Mac, Linux, iOS, Android | RDP, VNC, SSH     | :material-check:                    | :material-check:             | JPEG, PNG                     |
| noVNC Client             | Windows, Mac, Linux, iOS, Android | VNC               | :material-check:                    | :material-check:             | Tight, Hextile, CopyRect, Raw |


| VDI Viewer               | Features                                                                                                                                   | Pros                                                                                                                                                                          | Cons                                                                                                                      |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| Microsoft Remote Desktop | - RDP protocol <br> - Multimedia redirection <br> - USB redirection <br>- RemoteFX video compression                          | - Built-in support in Windows operating systems <br>- RemoteFX provides high-quality video and audio streaming <br>- Easy to set up and use                         | - Limited support for non-Windows operating systems <br>- Security concerns with RDP protocol                        |
| Spice                    | - SPICE protocol <br>- Multimedia redirection& <br>- USB redirection <br>- Lossless and lossy video compression              | - Open-source software with active development community <br>- Supports multiple operating systems <br>- Optimized for high-performance and low-latency             | - Limited multimedia support compared to other protocols <br>- Limited USB redirection compared to other protocols   |
| VNC                      | - VNC protocol <br>- USB redirection <br>- Tight and Zlib video compression                                                      | - Open-source software with active development community <br>- Supports multiple operating systems <br>- Easy to set up and use                                     | - Limited multimedia support compared to other protocols <br>- Limited security features compared to other protocols |
| Guacamole HTML5 Client   | - RDP, VNC, and SSH protocols <br>- Multimedia redirection <br>- USB redirection <br>- JPEG and PNG video compression       | - Accessible from any HTML5-compatible web browser <br>- Supports multiple protocols and operating systems <br>- No need to install client software on local device | - Requires installation and configuration of server software <br>- Limited performance compared to native clients    |
| noVNC Client             | - VNC protocol <br>- Multimedia redirection <br>- USB redirection <br>- Tight, Hextile, CopyRect, and Raw video compression | - Accessible from any HTML5-compatible web browser <br>- Supports multiple operating systems <br>- Supports multiple video compression formats                      | - Limited protocol support compared to other viewers <br>- Limited multimedia support compared to other protocols    |


## SPICE viewer

![](viewers.images/viewer10.png)

- Needs to have the client application installed:

    - *Windows*: [virt-viewer](https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi)
  - *Apple*: You can follow [this install guide](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)
  - *Linux*: `sudo apt install virt-viewer` / `sudo dnf install remote-viewer`
  - *Android*: There is a [free version](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) in Play Store and also a [payed one](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) with more features.
  - *Apple iOS*: [payed version](https://apps.apple.com/gb/app/aspice-pro/id1560593107) in Apple Store

- Low latency
- Audio integrated
- Optional USB attachment [info](#plug-devices-with-spice)

## Browser viewer

![](viewers.images/viewer1.png)

- Works with any modern browser
- Medium latency
- No audio available
- No local devices attachment available

## RDP

![](viewers.images/viewers_rdp.png)

!!! Info Dependencies
    Viewer available only when desktop started and IP address obtained

- Needs to have the client application installed (already installed in Windows)

    - *Windows*: [Remote Desktop clients](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)
    - *Mac OSX*: [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)
    - *Linux*: Remmina
    - *Android*: [Remote Desktop](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx)
    - *Apple iOS*: [Remote Desktop](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092)

- Low latency (best with Windows)
- Audio integrated
- Optional USB attachment [info](#plug-devices-with-rdp)
- Best when using 3D applications in Windows
- GPU desktops will only work with RDP clients.

For all RDP connections desktop should be [set up](#activate-rdp)

## RDP Browser

!!! Info Dependencies
    Viewer available only when desktop started and IP address obtained

- Works with any modern browser
- Low latency
- Audio integrated
- No local devices attachment available
- GPU desktops will only work with RDP clients.

## RDP VPN

!!! Info Dependencies
    Viewer available only when desktop started and IP address obtained

- Needs to have the client application installed (already installed in Windows)

    - *Windows*: [Remote Desktop clients](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)
    - *Mac OSX*: [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)
    - *Linux*: Remmina
    - *Android*: [Remote Desktop](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx)
    - *Apple iOS*: [Remote Desktop](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092)

- Needs to have [IsardVDI VPN](vpn.md) setup and connected
- Available when the desktop has started and gets an IP address
- Low latency (best with Windows)
- Audio integrated
- Optional USB attachment [info](#plug-devices-with-rdp)
- Best when using 3D applications in Windows
- GPU desktops will only work with RDP clients.

# Activate RDP

## Windows desktops

To allow RDP connections to your desktop you need to activate RDP in Windows and disable network authentication in advanced options in the windows guest.

1. Activate RDP

![](./viewers.images/guest_rdp_cfg1.png)

2. Disable network authentication

![](./viewers.images/guest_rdp_cfg2.png)

## Linux desktops

- https://nextcloud.isardvdi.com/s/XY4ow9P5NHASxmx

# Plug USB devices

## Plug devices with RDP

From the desktop that you want to connect the USB, press the button ![](./viewers.es.images/visor21.png)

![](./viewers.es.images/visor20.png)

You have to download the viewer file that you want to use with rdp

![](./viewers.es.images/visor22.png)

Open the file where you have the download and click on the "modify" button

![](./viewers.es.images/visor24.png)

Press the button "local resources"

![](./viewers.es.images/visor25.png)

And press the button "More" on local devices and resources

![](./viewers.es.images/visor26.png)

Once there, in the "Units" section we select the USB that we want, in my case I have selected the "SUN-USB(D:)"

![](./viewers.es.images/visor27.png)

The "Connect" button is pressed

![](./viewers.es.images/visor28.png)

![](./viewers.es.images/visor29.png)

User: isard and password: pirineus -> default

![](./viewers.es.images/visor30.png)

Press the button "yes"

![](./viewers.es.images/visor31.png)

And when you enter through the remote desktop, something like this should come out. In this case it's D because it's where it was on my computer.

![](./viewers.es.images/visor32.png)

## Plug devices with Spice

!!! Info Dependencies
    This applies to Windows only as in linux remote-viewer client this option is active by default.

!!! Important Dependencies
    Download the USB driver so that it can be detected in the viewer. (UsbDk_1.0.22x64.msi): https://www.spice-space.org/download.html**

Once the driver is downloaded, open the Spice viewer, press the "File" button

![](./viewers.es.images/visor33.png)

And the option "USB device selection" is selected

![](./viewers.es.images/visor34.png)

And the device you want to use is selected.

![](./viewers.es.images/visor35.png)

<hr>

# Viewers Technical details

We can differentiate two main types of viewers depending on the place from where we access:

* **Viewers integrated in the browser**: the viewer is integrated into a web page, from a browser tab we can manage a windows or a linux. The decoding of the video signal and the sending of the mouse and keyboard signal is done from a browser tab.

    * **Advantage**:
        * You do not need to have a client installed, it works from any device (computers, tablets, mobile phones) and operating system that has a browser.

    * **Disadvantages**:
        * Decoding is not as efficient as in a dedicated viewer, noticeable **slower** on desktop refresh and when moving items around the desktop.
        * We cannot redirect to the virtual desktop local devices connected by usb such as a pendrive, a webcam...

* **Desktop client applications**: these are applications dedicated to acting as viewers, they are optimized and are the best option to have the best user experience

    * **Advantage**:
        * Optimized for desktop client protocols, better performance than in-browser viewers. If there are no network problems (adequate latency and bandwidth) the feeling of working with the desktop is similar to that of a real desktop.
        * Although it depends on the type of protocol and the version of the client, in general they offer advanced options such as: redirection of USB ports, copy and paste from the virtual desktop to the real desktop, upload files through the viewer to the virtual desktop...

    * **Disadvantages**:
        * Not in all cases do they come pre-installed with the operating system, so it is necessary to carry out an installation on the operating system of the computer from which we connect.
        * Advanced features not available depending on client versions and protocol version

## Protocols and viewers available in Isard

* **SPICE**: is a communication protocol for virtual environments. It allows us to access the video signal, mouse and keyboard of the desktop as if we were connected to the screen, mouse and keyboard of a real computer.

    * **Advantage**:
        * It is the virtual machine engine that Isard uses (Qemu-KVM) that gives us access regardless of the operating system that is running.
        * We can use this protocol to see the entire desktop boot sequence, install operating systems...
        * We do not need to install any components in the virtual desktop operating system to be able to interact with it.
        * It is a protocol that optimizes the video bandwidth used by compressing the signal and sending only the areas that vary from one frame to another.

    * **Disadvantages**:
        * The client is not installed by default on any operating system, the installation is very simple and can be done by any user, but in corporate or  educational environments permission restrictions may make it difficult to install.
        * The installation in windows requires an extra program to be able to redirect the USB ports
        * The connection is made through an HTTP proxy using a "CONNECT" method. This method, in some cases, is filtered by some intermediate firewall or proxy.    

* **VNC**: is a protocol that works at the same level as SPICE but since it is older, the video signal is not as optimized. It is the protocol that we use in the client integrated in the browser

* **RDP**: is the protocol used by default to connect remotely to a windows operating system. It needs us to allow remote access in the operating system. It is not available from desktop boot and you have to wait for the desktop boot sequence to get an IP address. The main advantage is that it offers the best smooth experience for a windows desktop, using the current native RDP windows client. It is the recommended protocol also when used in conjunction with Nvidia vGPU cards.

    * **Advantage**:
        * The best user experience if the operating system of the virtual desktop is windows and the computer from which the client connects is also windows
        * In windows clients it is not necessary to install additional software, since the remote desktop client comes by default in all windows
        * It is necessary for a good user experience using Nvidia vGPUs on windows operating systems.

    * **Disadvantages**:
        * If there is a problem when starting the operating system, you do not access the screen signal (you solve it by connecting through spice or vnc)


## Ports and proxies

Isard has made a significant effort to avoid having to open extra ports and encapsulate connections in HTTP proxies. It is about adapting to any situation where there is a firewall that can make it difficult to connect. By default the ports are used:

- TCP/80 for the proxy where the SPICE protocol connections are encapsulated
- TCP/443 for web and browser-embedded viewers
- TCP/9999 for the proxy through which the RDP protocol connections are encapsulated


## Browser-embedded VNC viewer

To be able to see a desktop directly from the browser, select the option ![](viewers.images/viewer1.png)

![](viewers.images/viewer2.png)

!!! Important Dependencies
    It is necessary to take into account if the browser is blocked from opening pop-up windows

In Firefox: A message about popups will appear

![](viewers.images/viewer3.png)

Press the button ![](viewers.images/viewer13.png)

![](viewers.images/viewer4.png)

And it will open the desktop in a new browser tab

![](viewers.images/viewer5.png)

## Viewer for the SPICE protocol 

!!! Important Dependencies
    It is recommended to use [version 7](https://www.spice-space.org/download.html) of Spice.**

To be able to use the Spice viewer download the viewer client:

### Microsoft
#### Spice client app

* [Spice viewer client](https://releases.pagure.org/virt-viewer/virt-viewer-x64-9.0.msi)
    * [Spice viewer USB client for Windows](https://www.spice-space.org/download/windows/usbdk/UsbDk_1.0.22_x64.msi)

### Linux

#### Spice client app

* Debian/Ubuntu based distros: `sudo apt install virt-viewer -y`
* RedHat/CentOS/Fedora based distros: `sudo dnf install remote-viewer -y`

Once installed, when booting a desktop you can select the "Spice Viewer" option from the drop down menu

![](viewers.images/viewer10.png)

And a dialog box will appear so you can open the Spice file

![](viewers.images/viewer11.png)

Once opened, a window will appear with the desktop

![](viewers.images/viewer12.png)

## RDP viewer

!!! Important Dependencies
    For the RDP client to connect **It is necessary to have the Wireguard network activated on the desktop through which we will connect and have the "Remote Desktop" configured on said desktop**

On computers with windows operating system it is not necessary to install any additional software since they have the remote desktop client installed as a windows tool.

### RDP client app (Windows)

* Usually Windows already have `Remote Desktop Connection` installed. If not refer to [Remote Desktop clients](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-clients)

On computers with Linux you have to install a client like "Remmina" and on macOS there is an official application, "Microsoft Remote Desktop", which can be downloaded from the [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### RDP client app (Linux)

* In Linux distros the client app for RDP it is called `Remmina` and it can be installed following [this guide](https://remmina.org/how-to-install-remmina/)

As previously mentioned, the RDP viewer needs an IP to connect to, so when a desktop starts we see a blink and the links to the RDP viewers appear as non-selectable:

![](viewers.images/viewer14.png)

Once the IP address has been obtained, it is reported and access to viewers is activated:

![](viewers.images/viewer15.png)

Selecting the **RDP viewer** downloads a file with an .rdp extension:

![](viewers.images/viewer16.png)

The file isard-rdp-gw.rdp contains the information to be able to connect to the desktop.

!!! Info Dependencies
It is likely that the first time in windows operating systems it will inform us with a security alert, we can confirm it in the box "Do not ask me again about connections to this computer" and in the future it will not ask us anymore.

![](viewers.es.images/visor15.png)

Now it asks us for the credentials. It is important to note that this username and password is that of the virtual desktop that is running. In the templates we offer by default:

* user is: isard
* the password is: pirineus

![](viewers.es.images/visor18.png)

When accessing the desktop, it asks us for confirmation to accept the certificate:

![](viewers.es.images/visor19.png)

And finally the client opens and we can interact with the desktop.

## Download native clients viewers for other distributions

*For spice client app you can find more information for any OS at [spice-space](https://www.spice-space.org/download.html)*

### MacOS

#### Spice client app

* You can follow [this install guide](https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c)

#### RDP client app

* There is an official application, `Microsoft Remote Desktop`, which can be downloaded from the [Apple Store](https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12)

### Android

#### Spice client app

* There is a [free version](https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE) in Play Store and also a [payed one](https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE) with more features.

#### RDP client app

* Microsoft has it's own [Remote Desktop](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx) in Play Store

### IOS

#### Spice client app

* There is a [payed version](https://apps.apple.com/gb/app/aspice-pro/id1560593107) in Apple Store

#### RDP client app

* Microsoft has it's own [Remote Desktop](https://apps.apple.com/us/app/remote-desktop-mobile/id714464092) in Apple Store


### RDP viewers

You can activate multiple RDP connections when creating or editing a desktop:

![](./viewers.images/viewer_types.png)

- RDP web (HTML5)
- RDP (native client through IsardVDI proxy at default port tcp/9999)
- RDP vpn (through wireguard vpn)

!!! Important Dependencies
    All this viewers need that the desktop has the `Wireguard VPN` interface alongside others.

#### RDP HTML5

IsardVDI uses guacamole server that allows HTML5 RDP clients (any actual browser) to connect to IsardVDI windows guests through the default https port. Also it has audio through the browser.

#### RDP Native

You only need the client (RDP client in Windows or Remmina in Linux) and that the desktop has the `Wireguard` interface added.

This connection is done by default throught the tcp/9999 port but you can change it at `isardvdi.cfg` if you need to.

#### RDP VPN

You need also the client and the `Wireguard` interface added to the desktop.

To connect through wireguard vpn to RDP you'll need to set up your `isard-vpn.conf` wireguard client file in your client computer. Refer to ![using vpn](https://isard.gitlab.io/isardvdi-docs/user/vpn/) in IsardVDI.


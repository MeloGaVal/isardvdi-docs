# Introducció

El manual d'usuari s'organitza seguint l'ordre en el menú web IsardVDI, i consisteix en:

- Els mètodes d'**Inici de sessió** disponibles per a accedir al sistema.
- **Escriptoris**, les seves accions i visors.
- **Plantilles** que permeten ser replicades com a múltiples escriptoris.
- Es poden pujar **Mèdia**, normalment una ISO per a crear un escriptori nou.
- **Desplegaments** permet supervisar els escriptoris mentre hi treballeu.
- Es necessiten **Reserves** per a recursos com la vGPU.
- **Emmagatzematge** mostra quant s'utilitzen els vostres discs d'escriptori.
- **Administració** és un enllaç que permet als gestors i administradors fer funcions avançades.
- **Perfil** no només mostra la vostra informació i permet restablir la contrasenya, també mostra la vostra quota i ús.

![](index.images/global_view.png)
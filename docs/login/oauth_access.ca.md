# Accés mitjançant OAuth

Per a poder accedir, per exemple des d'un compte de Google, mitjançant un codi de registre proporcionat per un Manager.

Des de la pàgina principal és posbile accedir mitjançant un compte de correu de Google:

![](./oauth_access.ca.images/login1.png)

Es prem el botó ![](./oauth_access.ca.images/codi3.png) de la pantalla principal d'IsardVDI.

Demanarà un compte de correu de Google:

![](./oauth_access.ca.images/google2.png)

S'escriu el compte de correu i es prem el botó de "Següent" i sol·licitarà la contrasenya del compte de Google:

![](./oauth_access.ca.images/google2.png)

Una vegada introduïda, es prem el botó "Següent".


## Inscripció per codi de registre

Si és la primera vegada que s'accedeix com a usuari apareixerà el formulari per a introduir un codi de registre. 

El codi de registre es crea per part del manager de l'organització i s'explica com es genera més endavant en la secció de "Manager" de la documentació. Cada usuari ha de pertànyer a un grup i tenir un rol, ja sigui advanced (professor), user (alumne), etc. Per cada grup d'usuaris es poden generar codis d'autoregistre per a diferents rols.

S'introdueix el codi proporcionat per un Manager i es prem el botó ![](./oauth_access.ca.images/codi2.png)

![](./oauth_access.ca.images/codi1.png)

I s'accedeix a la interfície bàsica autenticats amb el compte de Google.

![](./oauth_access.ca.images/home1.png)

Una vegada autenticats, ja no fa falta repetir el procés de registre. Quan es vulgui tornar a accedir mitjançant Google, només s'ha de prémer en el botó ![](./oauth_access.ca.images/google1.png) i utilitzar les credencials de Google.
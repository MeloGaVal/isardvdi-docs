# Accés Local

S'han passat una sèrie d'usuaris i contrasenyes d'usuaris locals (no es validen per Google). Si s'accedeix a https://DOMINI cal seleccionar la categoria i l'idioma:

![](./local_access.ca.images/local_access1.png)

Si s'accedeix tal com s'ha recomanat a la URL de login del centre (https://DOMINI/login/categoria), només és necessari triar l'idioma:

![](./local_access.ca.images/local_access2.png)

L'elecció de l'idioma es queda guardada en un cookie en el navegador, i la pròxima vegada que entris ja apareixerà preseleccionada amb l'idioma que vas triar l'última vegada.
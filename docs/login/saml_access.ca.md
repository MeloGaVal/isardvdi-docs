# Accés SAML

Per a accedir utilitzant un compte de SAML, primer de tot necessites tenir un codi de registre, que t'hauria de donar un Manager.

![](./saml_access.ca.images/login.png)

En primer lloc, has de fer click al botó ![](./saml_access.images/button.png) que està a la pàgina principal d'inici de sessió d'IsardVDI

A partir d'aquí, continua amb el procés d'inici de sessió. Aquesta pàgina és diferent segons cada servidor SAML. Si utilitzes l'inici de sessió de SAML per algun altre servei, segurament ja coneixeràs com funciona.

## Inscripció per codi de registre

Si és la primera vegada que s'accedeix com a usuari apareixerà el formulari per a introduir un codi de registre. 

El codi de registre es crea per part del manager de l'organització i s'explica com es genera més endavant en la secció de "Manager" de la documentació. Cada usuari ha de pertànyer a un grup i tenir un rol, ja sigui advanced (professor), user (alumne), etc. Per cada grup d'usuaris es poden generar codis d'autoregistre per a diferents rols.

S'introdueix el codi proporcionat per un Manager i es prem el botó ![](./oauth_access.ca.images/codi2.png)

![](./oauth_access.ca.images/codi1.png)

I s'accedeix a la interfície bàsica autenticats amb el compte de SAML.

![](./oauth_access.ca.images/home1.png)

Una vegada autenticats, ja no fa falta repetir el procés de registre. Quan es vulgui tornar a accedir mitjançant SAML, només s'ha de prémer en el botó ![](./saml_access.images/button.png) i utilitzar les credencials de SAML.
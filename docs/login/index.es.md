# Introducción

Para acceder al servidor tenéis que ir a https://isardvdi_domain y se os presentará la página de inicio de sesión.

![](./local_access.images/login.png)

Vuestra cuenta de usuario podría ser de uno de estos tipo:

- **local**: Como usuario os tienen que haber proporcionado vuestra *categoría* y credenciales de usuario.
- **OAuth2** (*google/gitlab*): Se os tiene que haber proporcionado un *código de acceso* necesario para completar el registro con vuestra cuenta personal.
- **SAML**: Utilizáis vuestras credenciales corporativas para iniciar la sesión. También os tienen que haber proporcionado un *código de acceso* para completar el autorregistro.

Cuando utilizáis la autenticación **local**, podéis evitar tener que seleccionar la categoría cada vez yendo directamente al identificador de vuestra categoría (preguntáis a vuestro gestor IsardVDI) adjuntandolo a la URL de inicio de sesión de este modo:

- https://isardvdi_domain/login/category_id

A continuación, la página de inicio de sesión seleccionará la categoría automáticamente y no hay que introducirla cada vez.
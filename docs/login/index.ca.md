# Introducció

Per accedir al servidor heu d'anar a https://isardvdi_domain i se us presentarà la pàgina d'inici de sessió.

![](./local_access.images/login.png)

El vostre compte d'usuari podria ser d'un d'aquest tipus:

- **local**: Com a usuari us han d'haver proporcionat la vostra *categoria* i credencials d'usuari.
- **OAuth2** (*google/gitlab*): Se us ha d'haver proporcionat un *codi d'accés* necessari per a completar el registre amb el vostre compte personal.
- **SAML**: Utilitzeu les vostres credencials corporatives per a iniciar la sessió. També us han d'haver proporcionat un *codi d'accés* per a completar l'autoregistre.

Quan utilitzeu l'autenticació **local**, podeu evitar haver de seleccionar la categoria cada vegada anant directament a l'identificador de la vostra categoria (pregunteu al vostre gestor IsardVDI) adjuntat a l'URL d'inici de sessió d'aquesta manera:

- https://isardvdi_domain/login/category_id

A continuació, la pàgina d'inici de sessió seleccionarà la categoria automàticament i no cal introduir-la cada vegada.
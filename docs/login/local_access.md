# Local Access

A series of usernames and passwords of local users have been passed (they are not validated by Google). If you access https://DOMAIN, you must select the category and language:

![](./local_access.images/local_access1.png)

If the login URL of the center is accessed as recommended (https://DOMINIO/login/category), it is only necessary to choose the language:

![](./local_access.images/local_access2.png)

The choice of language is saved in a cookie in the browser, and the next time you enter it will appear preselected with the language you chose the last time.
# SAML Access

To access using a SAML account, you need to first get a registration code, which will be provided by a Manager.

![](./saml_access.images/login.png)

First you need to press the button ![](./saml_access.images/button.png) from the main IsardVDI login screen

Now, continue through the login process. This page is different between each SAML implementation. If you use SAML for another service, you'll probably already know how it works.

## Registration by registration code

If this is the first time you are accessing as a user, a form will appear to enter a registration code.

The registration code is created by the organization manager and how it is generated is explained later in the "Manager" section of the documentation. Each user must belong to a group and have a role, either advanced (teacher), user (student), etc. For each group of users, self-registration codes can be generated for different roles.

Enter the code provided by a Manager and press the button ![](./oauth_access.images/code2.png)

![](./oauth_access.images/code1.png)

And you access the basic interface authenticated with the SAML account.

![](./oauth_access.images/home1.png)

Once authenticated, there is no need to repeat the registration process. When you want to access again through SAML, just click on the button ![](./saml_access.images/button.png)
# Acceso mediante OAuth

Para poder acceder, por ejemplo desde una cuenta de Google, mediante un código de registro proporcionado por un Manager.

Desde la página principal es posbile acceder mediante una cuenta de correo de Google.

![](./oauth_access.es.images/login1.png)

Se pulsa el botón ![](./oauth_access.ca.images/codi3.png) de la pantalla principal de IsardVDI

Pedirá una cuenta de correo de Google

![](./oauth_access.es.images/google1.png)

Se escribe la cuenta de correo y se pulsa el botón de "Siguiente" y solicitará la contraseña de la cuenta de Google.

![](./oauth_access.es.images/google2.png)

Una vez introducida, se pulsa el botón "Siguiente". 

## Inscripción por código de registro

Si es la primera vez que se accede como usuario aparecerá el formulario para introducir un código de registro. 

El código de registro se crea por parte del manager de la organización y se explica cómo se genera más adelante en la sección de "Manager" de la documentación. Cada usuario ha de pertenecer a un grupo y tener un rol, ya sea advanced (profesor), user (alumno), etc. Por cada grupo de usuarios se pueden generar códigos de autoregistro para diferentes roles.

Se introduce el código proporcionado por un Manager y se pulsa el botón ![](./oauth_access.es.images/codigo2.png)

![](./oauth_access.es.images/codigo1.png)

Y se accede a la interfaz básica autenticados con la cuenta de Google.

![](./oauth_access.es.images/home1.png)

Una vez autenticados, ya no hace falta repetir el proceso de registro. Cuando se quiera volver a acceder mediante Google, sólo se tiene que pulsar en el botón ![](./oauth_access.ca.images/google1.png) y utilizar las credenciales de Google.